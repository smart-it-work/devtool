package cn.devtool.monitor.test;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.ClassFileLocator;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.matcher.ElementMatchers;
import net.bytebuddy.pool.TypePool;

import java.io.IOException;

/**
 * HelloWorld示例
 * @author chentiefeng
 * @created 2021/7/21 12:04
 */
public class FooTest {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, IOException, NoSuchFieldException {
        Foo dynamicFoo = new ByteBuddy()
                .subclass(Foo.class)
                .method(ElementMatchers.isDeclaredBy(Foo.class)).intercept(FixedValue.value("One!"))
                .method(ElementMatchers.named("foo")).intercept(FixedValue.value("Two!"))
                .method(ElementMatchers.named("foo").and(ElementMatchers.takesArguments(1))).intercept(FixedValue.value("Three!"))
                .make()
                .load(FooTest.class.getClassLoader())
                .getLoaded()
                .newInstance();
        System.out.println(dynamicFoo.foo()); // bar
    }
}

class Foo {

    public boolean foo() {
        return false;
    }
}
