package cn.devtool.monitor.test;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.implementation.Implementation;
import net.bytebuddy.matcher.ElementMatchers;

/**
 * HelloWorld示例
 * @author chentiefeng
 * @created 2021/7/21 12:04
 */
public class HelloWorld {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        // 需要修改的类
        Class<Object> clazz = Object.class;
        // 需要修改的方法名
        String methodName = "toString";
        // 方法中添加的执行
        Implementation implementation = FixedValue.value("Hello World!");
        // 执行的类加载器
        ClassLoader classLoader = HelloWorld.class.getClassLoader();
        Class<?> dynamicType = new ByteBuddy()
                .subclass(clazz)
                .method(ElementMatchers.named(methodName))
                .intercept(implementation)
                .make()
                .load(classLoader)
                .getLoaded();

        // 获取类的实例
        Object object = dynamicType.newInstance();
        System.out.println(object.toString());
    }
}
