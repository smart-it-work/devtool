package cn.devtool.monitor.test;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.implementation.Implementation;
import net.bytebuddy.matcher.ElementMatchers;

import java.io.File;
import java.io.IOException;

/**
 * HelloWorld示例
 * @author chentiefeng
 * @created 2021/7/21 12:04
 */
public class UserServiceTest {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, IOException {
        // 需要修改的类
        Class<UserService> clazz = UserService.class;
        // 需要修改的方法名
        String methodName = "getUserId";
        // 方法中添加的执行
        Implementation implementation = FixedValue.value("Hello World!");
        // 执行的类加载器
        ClassLoader classLoader = UserServiceTest.class.getClassLoader();
        DynamicType.Unloaded<UserService> serviceUnloaded = new ByteBuddy()
                .redefine(clazz)
                .method(ElementMatchers.named(methodName))
                .intercept(implementation)
                .name("UserServiceImpl")
                .make();
        serviceUnloaded.saveIn(new File("C:\\Users\\imche\\Desktop\\tmpdata", "test"));
//        DynamicType.Loaded<UserService> serviceLoaded = serviceUnloaded.load(classLoader);
//        Class<? extends UserService> newClazz = serviceLoaded.getLoaded();
//        System.out.println(newClazz.newInstance().getUserId());
    }
}
