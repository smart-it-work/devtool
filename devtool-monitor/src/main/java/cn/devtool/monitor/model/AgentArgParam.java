package cn.devtool.monitor.model;

import lombok.Data;

/**
 * Agent参数
 *
 * @author chentiefeng
 * @created 2021/7/21 18:16
 */
@Data
public class AgentArgParam {
    /**
     * 包的根路径
     */
    private String basePackage;

    /**
     * 方法
     */
    private String method;
}
