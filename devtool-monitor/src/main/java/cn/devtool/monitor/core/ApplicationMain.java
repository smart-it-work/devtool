package cn.devtool.monitor.core;


import cn.devtool.monitor.model.AgentArgParam;

/**
 * 程序的入口
 *
 * @author chentiefeng
 * @created 2021/7/21 11:59
 */
public class ApplicationMain {
    /**
     * 相当于Main方法，是监控插件的主入口
     * @param agentArgParam agent参数
     */
    public static void premain(AgentArgParam agentArgParam) {

    }
}
