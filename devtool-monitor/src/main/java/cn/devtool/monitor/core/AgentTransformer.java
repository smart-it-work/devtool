package cn.devtool.monitor.core;

import cn.devtool.monitor.model.AgentArgParam;
import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.utility.JavaModule;

/**
 * Agent Transformer
 *
 * @author chentiefeng
 * @created 2021/7/21 17:44
 */
public class AgentTransformer implements AgentBuilder.Transformer {
    /**
     * Agent参数
     */
    private final AgentArgParam agentArgParam;
    public AgentTransformer(AgentArgParam agentArgParam) {
        this.agentArgParam = agentArgParam;
    }
    @Override
    public DynamicType.Builder<?> transform(DynamicType.Builder<?> builder, TypeDescription typeDescription, ClassLoader classLoader, JavaModule javaModule) {
        return builder
                .method(ElementMatchersUtils.getMethodMatcherByName(agentArgParam.getMethod()))
                .intercept(ImplementationUtils.getImplementationByArgs(agentArgParam));
    }
}
