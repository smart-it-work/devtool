package cn.devtool.monitor.core;

import cn.devtool.monitor.model.AgentArgParam;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.implementation.Implementation;

/**
 * Implementation工具类
 *
 * @author chentiefeng
 * @created 2021/7/21 18:01
 */
public class ImplementationUtils {
    /**
     * 通过Agent客户端参数获取 Implementation
     * @param agentArgParam Agent客户端参数
     * @return 返回Implementation
     */
    public static Implementation getImplementationByArgs(AgentArgParam agentArgParam) {

        // return MethodDelegation.to(MonitorMethod.class); // 委托
        return FixedValue.value("transformed");
    }
}
