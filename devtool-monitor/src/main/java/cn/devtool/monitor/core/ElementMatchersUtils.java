package cn.devtool.monitor.core;

import cn.devtool.monitor.model.AgentArgParam;
import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;

import java.lang.annotation.Annotation;

/**
 * @author chentiefeng
 * @created 2021/7/21 17:46
 */
public class ElementMatchersUtils {
    /**
     * 通过Agent参数获取Matcher
     * @param agentArgParam Agent参数
     * @return 匹配的ElementMatcher
     */
    public static ElementMatcher<? super TypeDescription> getTypeMatcherByAgentArgs(AgentArgParam agentArgParam) {
        // TODO 从参数读取需要过滤的参数
        return getTypeMatcherByNameStart("cn.devtool.test");
    }

    /**
     * 通过注解类获取Matcher
     * @param annotatedClass 注解类
     * @return 匹配的ElementMatcher
     */
    public static ElementMatcher<? super TypeDescription> getTypeMatcherByAnnotated(Class<? extends Annotation> annotatedClass) {
        return ElementMatchers.isAnnotatedWith(annotatedClass);
    }

    /**
     * 通过name开头找寻 Matcher
     * @param prefix name开头
     * @return 匹配的Matcher
     */
    public static ElementMatcher<? super TypeDescription> getTypeMatcherByNameStart(String prefix) {
       return   ElementMatchers.nameStartsWith(prefix);
    }

    /**
     * 查询方法匹配
     * @param methodName 方法名称
     * @return 匹配的Matcher
     */
    public static ElementMatcher<? super MethodDescription> getMethodMatcherByName(String methodName) {
        return ElementMatchers.named(methodName);
    }
}
