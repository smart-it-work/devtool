package cn.devtool.monitor;

import cn.devtool.monitor.core.AgentTransformer;
import cn.devtool.monitor.core.ApplicationMain;
import cn.devtool.monitor.core.ElementMatchersUtils;
import cn.devtool.monitor.model.AgentArgParam;
import cn.devtool.monitor.util.AgentArgsUtils;
import net.bytebuddy.agent.builder.AgentBuilder;

import java.lang.instrument.Instrumentation;

/**
 * Monitor Agent入口类
 *
 * @author chentiefeng
 * @created 2021/7/21 14:28
 */
public class MonitorAgent {
    private MonitorAgent() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 相当于Main方法，是监控插件的主入口
     * @param agentArgs agent参数
     * @param instrumentation Instrumentation
     */
    public static void premain(String agentArgs, Instrumentation instrumentation) {
        AgentArgParam agentArgParam = AgentArgsUtils.parse(agentArgs);
        new AgentBuilder.Default()
                .type(ElementMatchersUtils.getTypeMatcherByAgentArgs(agentArgParam))
                .transform(new AgentTransformer(agentArgParam)).installOn(instrumentation);
        ApplicationMain.premain(agentArgParam);
    }
}
