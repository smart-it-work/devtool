package cn.devtool.jpa.util;

import cn.devtool.jpa.model.PageDto;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Jpa分页对象工具类
 *
 * @author chentiefeng
 * @created 2021/7/14 10:52
 */
public class JpaPageUtils {
    private JpaPageUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 封装查询的结果为PageDto对象
     * @param pageData 分页数据
     * @param currentPage 当前页
     * @param pageSize 每页大小
     * @param <T> 结果对象泛型
     * @return PageDto对象
     */
    public static <T> PageDto<T> createPageDto(Page<T> pageData, int currentPage, int pageSize) {
        return createPageDto(pageData.getContent(), pageData.getTotalPages(), pageData.getTotalElements(), currentPage, pageSize);
    }

    /**
     * 封装查询的结果为PageDto对象
     * @param dataList 数据集合
     * @param totalPage 总页数
     * @param totalCount 总条数
     * @param currentPage 当前页
     * @param pageSize 每页大小
     * @param <T> 结果对象泛型
     * @return PageDto对象
     */
    public static <T> PageDto<T> createPageDto(List<T> dataList, int totalPage, long totalCount, int currentPage, int pageSize) {
        PageDto<T> result = new PageDto<>();
        result.setTotal(totalCount);
        result.setTotalPages(totalPage);
        result.setPageCurrent(currentPage);
        result.setPageSize(pageSize);
        result.setDataList(dataList);
        return result;
    }
}
