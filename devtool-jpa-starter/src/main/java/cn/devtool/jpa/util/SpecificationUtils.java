package cn.devtool.jpa.util;

import cn.devtool.core.util.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Specification工具类
 *
 * @author chentiefeng
 * @created 2021/7/14 10:28
 */
public class SpecificationUtils {
    private SpecificationUtils() {
        throw new IllegalStateException("Utility class");
    }
    public static final String VALUE_SPLITTER = ",";
    /**
     * 创建Specification
     * @param predicateFun Predicate的构建函数
     * @param <T> Model类型
     * @return Model的Specification
     */
    public static  <T> Specification<T> createSpecification(BiFunction<Root<T>, CriteriaBuilder, Predicate> ... predicateFun) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            for (BiFunction<Root<T>, CriteriaBuilder, Predicate> tmpPredicateFun : predicateFun) {
                predicates.add(tmpPredicateFun.apply(root, criteriaBuilder));
            }
            // 添加查询条件
            return criteriaBuilder.and(predicates.toArray(new javax.persistence.criteria.Predicate[0]));
        };
    }



    public static void notNull(String fieldValue, List<Predicate> predicates, Function<String, Predicate> function) {
        if (StringUtils.nonBlank(fieldValue)) {
            predicates.add(function.apply(fieldValue));
        }
    }
    public static void notNull(Date fieldValue, List<Predicate> predicates, Function<Date, Predicate> function) {
        if (fieldValue != null) {
            predicates.add(function.apply(fieldValue));
        }
    }
    public static void notNull(Collection<String> fieldValueCollection, List<Predicate> predicates, Function<Collection<String>, Predicate> function) {
        if (!CollectionUtils.isEmpty(fieldValueCollection)) {
            predicates.add(function.apply(fieldValueCollection));
        }
    }


    public static void notNullIn(String fieldName, Collection<String> fieldValueCollection, List<Predicate> predicates, Root root) {
        notNull(fieldValueCollection, predicates, item -> root.get(fieldName).in(item));
    }
    public static void notNullNotIn(String fieldName, Collection<String> fieldValueCollection, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        if (!CollectionUtils.isEmpty(fieldValueCollection)) {
            predicates.add(criteriaBuilder.and(criteriaBuilder.not(criteriaBuilder.in(root.get(fieldName)).value(fieldValueCollection))));
        }
    }
    /**
     * 如果不为空，添加等待条件
     * @param fieldName 字段名
     * @param fieldValue 字段值
     * @param predicates Predicate集合
     * @param root root
     * @param criteriaBuilder CriteriaBuilder
     */
    public static void notNullEqual(String fieldName, String fieldValue, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        notNull(fieldValue, predicates, item -> criteriaBuilder.equal(root.get(fieldName), item));
    }

    public static void notNullGreaterThanOrEqualTo(String fieldName, Date fieldValue, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        notNull(fieldValue, predicates, item -> criteriaBuilder.greaterThanOrEqualTo(root.get(fieldName), item));
    }

    public static void notNullGreaterThanOrEqualTo(String fieldName, String fieldValue, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        notNull(fieldValue, predicates, item -> criteriaBuilder.greaterThanOrEqualTo(root.get(fieldName), item));
    }

    public static void notNullLessThanOrEqualTo(String fieldName, Date fieldValue, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        notNull(fieldValue, predicates, item -> criteriaBuilder.lessThanOrEqualTo(root.get(fieldName), item));
    }

    public static void notNullLessThanOrEqualTo(String fieldName, String fieldValue, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        notNull(fieldValue, predicates, item -> criteriaBuilder.lessThanOrEqualTo(root.get(fieldName), item));
    }
    /**
     * 关联字段的等待条件
     * @param fieldName 字段名
     * @param fieldValue 字段值
     * @param fieldValue2 字段值2
     * @param predicates Predicate集合
     * @param root root
     * @param criteriaBuilder CriteriaBuilder
     */
    public static void notNullLinkValueEqual(String fieldName, String fieldValue, String fieldValue2, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        notNullLinkValueEqual(false, fieldName, fieldValue, fieldValue2, predicates, root, criteriaBuilder);
    }
    /**
     * 关联字段的等待条件
     * @param inFlag 考虑in情况
     * @param fieldName 字段名
     * @param fieldValue 字段值
     * @param fieldValue2 字段值2
     * @param predicates Predicate集合
     * @param root root
     * @param criteriaBuilder CriteriaBuilder
     */
    public static void notNullLinkValueEqual(boolean inFlag, String fieldName, String fieldValue, String fieldValue2, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        if (StringUtils.nonBlank(fieldValue)) {
            if (inFlag && fieldValue.contains(VALUE_SPLITTER)) {
                predicates.add(root.get(fieldName).in(Arrays.asList(fieldValue.split(VALUE_SPLITTER))));
            } else {
                if (StringUtils.nonBlank(fieldValue2)) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(fieldName), fieldValue));
                } else {
                    predicates.add(criteriaBuilder.equal(root.get(fieldName), fieldValue));
                }

            }
        }
        if (StringUtils.nonBlank(fieldValue2)) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(fieldName), fieldValue2));
        }
    }

    public static void notNullLinkValueEqual(String fieldName, Date fieldValue, Date fieldValue2, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        if (fieldValue != null) {
            if (fieldValue2 != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(fieldName), fieldValue));
            } else {
                predicates.add(criteriaBuilder.equal(root.get(fieldName), fieldValue));
            }
        }
        if (fieldValue2 != null) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(fieldName), fieldValue2));
        }
    }

    public static void notNullLinkValueEqual(String fieldName, Integer fieldValue, Integer fieldValue2, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        if (fieldValue > 0) {
            if (fieldValue2 > 0) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(fieldName), fieldValue));
            } else {
                predicates.add(criteriaBuilder.equal(root.get(fieldName), fieldValue));
            }
        }
        if (fieldValue2 > 0) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(fieldName), fieldValue2));
        }
    }

    /**
     * 不未空的话，田间like语句
     * @param fieldName 字段名
     * @param fieldValue 字段值
     * @param predicates Predicate集合
     * @param root root
     * @param criteriaBuilder CriteriaBuilder
     */
    public static void notNullLike(String fieldName, String fieldValue, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        notNull(fieldValue, predicates, item -> criteriaBuilder.like(root.get(fieldName), "%" + item + "%"));
    }

    public static  void notNullNotEqual(String fieldName, String fieldValue, List<Predicate> predicates, Root root, CriteriaBuilder criteriaBuilder) {
        notNull(fieldValue, predicates, item -> criteriaBuilder.notEqual(root.get(fieldName), item));
    }
}
