package cn.devtool.jpa.util;

import cn.devtool.core.util.ClassUtils;

import java.util.List;

/**
 * Sql组装工具类
 *
 * @author chentiefeng
 * @created 2020-10-21 14:25
 */
public class SqlBuilder {
    private SqlBuilder() {}
    public static final String SPLIT_CHAR = ",";

    /**
     * 创建查询语句
     * @param columnNames 列名集合
     * @param tableName 表名
     * @param distinctFlag 是否是DISTINCT
     * @return 查询语句
     */
    public static StringBuilder crateSelectSql(List<String> columnNames, String tableName, boolean distinctFlag) {
        StringBuilder querySql = new StringBuilder(SqlField.SELECT);
        if (distinctFlag) {
            querySql.append(SqlField.DISTINCT);
        }
        querySql.append(String.join(SqlField.COLUMN_SPLIT, columnNames));
        querySql.append(SqlField.FROM);
        querySql.append(tableName);
        return querySql;
    }

    /**
     * 根据类创建查询所有字段的查询SQL.
     * @param clazz 结果对应类
     * @param tableName 表名
     * @param <T> 数据泛型
     * @return 查询所有的sql,如：select id，name from table_name
     */
    public static <T> StringBuilder crateSelectAllSql(Class<T> clazz, String tableName) {
        return crateSelectAllSql(clazz, tableName, false);
    }
    /**
     * 根据类创建查询所有字段的查询SQL.
     * @param clazz 结果对应类
     * @param tableName 表名
     * @param distinctFlag 是否是DISTINCT
     * @param <T> Class泛型
     * @return 查询所有的sql,如：select id，name from table_name
     */
    public static <T> StringBuilder crateSelectAllSql(Class<T> clazz, String tableName, boolean distinctFlag) {
        return crateSelectSql(ClassUtils.getUnderscoreFields(clazz), tableName, distinctFlag);
    }

    /**
     * 根据列名创建sql查询条件
     * @param columnName 列名
     * @return 返回列名的sql条件，如：column_name = ?
     */
    public static StringBuilder createSqlConditions(String columnName) {
        return createSqlConditions(columnName, columnName);
    }

    public static StringBuilder createSqlConditions(String columnName, String paramName) {
        return createEqualSqlConditions(columnName, paramName);
    }

    public static StringBuilder createEqualSqlConditions(String columnName, String paramName) {
        StringBuilder sqlConditions = new StringBuilder(" ");
        sqlConditions.append(columnName);
        sqlConditions.append(" = ");
        sqlConditions.append(":");
        sqlConditions.append(paramName);
        return sqlConditions;
    }

    public static StringBuilder createLikeSqlConditions(String columnName, String paramName) {
        StringBuilder sqlConditions = new StringBuilder(" ");
        sqlConditions.append(columnName);
        sqlConditions.append(" like ");
        sqlConditions.append(":");
        sqlConditions.append(paramName);
        return sqlConditions;
    }

    public static StringBuilder createInSqlConditions(String columnName, String paramName) {
        StringBuilder sqlConditions = new StringBuilder(" ");
        sqlConditions.append(columnName);
        sqlConditions.append(" in ");
        sqlConditions.append("(:").append(paramName).append(")");
        return sqlConditions;
    }

    public interface SqlField {
        String SELECT = "SELECT ";
        String DISTINCT = " DISTINCT ";
        String FROM = " FROM ";
        String WHERE = " WHERE ";
        String AND = " AND ";
        String OR = " OR ";
        String LEFT_PARENTHESIS = " ( ";
        String RIGHT_PARENTHESIS = " ) ";
        String LIKE = "%";
        String COLUMN_SPLIT = " , ";
        String ORDER_BY = " ORDER BY ";
        String ORDER_ASC = " ASC ";
    }
}
