package cn.devtool.jpa.util;

import cn.devtool.core.util.StringUtils;
import cn.devtool.jpa.annotation.JpaAnnotation;
import cn.devtool.jpa.annotation.UniqueKeyField;
import cn.devtool.jpa.handler.IAnnotationHandler;
import cn.devtool.jpa.model.PageDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.persistence.criteria.Predicate;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Jpa查询工具类
 *
 * @author chentiefeng
 * @created 2021/7/14 10:43
 */
@Slf4j
public class JpaQueryUtils<T, P> {

    /**
     * 查询所有数据
     * @param repository  repository
     * @param tSearchCommand 查询条件
     * @param currentPage 当前的页码
     * @param pageSize 每页大小
     * @param sortBy 排序
     * @param sortOrder 排序方式
     * @param <T> 数据泛型
     * @return 分页数据
     */
    public  <T> PageDto<T> searchAll(JpaSpecificationExecutor<T> repository, P tSearchCommand, int currentPage, int pageSize, String sortBy, String sortOrder) {
        Page<T> pageData = searchAllByPage(repository, tSearchCommand, currentPage, pageSize, sortBy, sortOrder);
        //转换数据并返回
        return JpaPageUtils.createPageDto(pageData, currentPage, pageSize);
    }
    /**
     *
     * @param repository repository
     * @param tSearchCommand 查询条件
     * @param <T> 数据泛型
     * @return 分页数据
     */
    /**
     * 根据条件查询所有
     * @param repository repository
     * @param tSearchCommand 查询条件
     * @param currentPage 当前的页码
     * @param pageSize 每页大小
     * @param sortBy 排序
     * @param sortOrder 排序方式
     * @param <T> 数据泛型
     * @return 分页数据
     */
    public  <T> Page<T> searchAllByPage(JpaSpecificationExecutor<T> repository, P tSearchCommand, int currentPage, int pageSize, String sortBy, String sortOrder) {
        Sort sort = null;
        //排序对象
        if (sortBy.contains(SqlBuilder.SPLIT_CHAR)) {
            //sort = Sort.by("DESC".equalsIgnoreCase(sortOrder) ? Sort.Direction.DESC : Sort.Direction.ASC, sortBy.split(SqlBuilder.SPLIT_CHAR));
        } else {
            //sort = Sort.by("DESC".equalsIgnoreCase(sortOrder) ? Sort.Direction.DESC : Sort.Direction.ASC, sortBy);
        }
        //分页对象
        //Pageable pageable = PageRequest.of(currentPage, pageSize, sort);
        //查询数据
        //return repository.findAll((Specification<T>) createQueryPredicate(tSearchCommand), pageable);
        return null;
    }
    /**
     * 根据条件查询所有
     * @param repository repository
     * @param tSearchCommand 查询条件
     * @return 数据集合
     */
    public List<T> searchAll(JpaSpecificationExecutor<T> repository, P tSearchCommand) {
        return repository.findAll((Specification<T>) createQueryPredicate(tSearchCommand));
    }

    /**
     * 根据注解创建查询条件
     * @param tSearchCommand 查询条件
     * @return Specification
     */
    public Specification<T> createQueryPredicate(P tSearchCommand) {
        Class clazz = tSearchCommand.getClass();
        Field[] fields = clazz.getDeclaredFields();
        return (Specification<T>) (root, query, criteriaBuilder) -> {
            //查询条件集合
            List<Predicate> predicates = new ArrayList<>();
            for (Field tmpField:fields) {
                Annotation[] annotations = tmpField.getAnnotations();
                for (Annotation annotation:annotations) {
                    JpaAnnotation jpaAnnotation = annotation.annotationType().getAnnotation(JpaAnnotation.class);
                    if (jpaAnnotation != null) {
                        Class<? extends IAnnotationHandler>  handlerClass = jpaAnnotation.handler();
                        try {
                            IAnnotationHandler annotationHandler = handlerClass.newInstance();
                            annotationHandler.handle(tmpField, tSearchCommand, predicates, root, query, criteriaBuilder);
                        } catch (Exception e) {
                            log.error("注解扫描创建" + tSearchCommand.getClass().getName() + "的查询条件出现异常！", e);
                        }
                    }
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    /**
     * 获取具有 @UniqueKeyField 注解的查询
     * @param obj 数据
     * @param <T> 数据泛型
     * @return Specification
     */
    public static <T> Specification<T> createUniqueKeyQueryPredicate(T obj) {
        Class clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        return (Specification<T>) (root, query, criteriaBuilder) -> {
            //查询条件集合
            List<Predicate> predicates = new ArrayList<>();
            for (Field tmpField:fields) {
                UniqueKeyField uniqueKeyField = tmpField.getAnnotation(UniqueKeyField.class);
                if (Objects.nonNull(uniqueKeyField)) {
                    try {
                        tmpField.setAccessible(true);
                        String fieldValue = (String) tmpField.get(obj);
                        if (StringUtils.nonBlank(fieldValue)) {
                            predicates.add(criteriaBuilder.equal(root.get(tmpField.getName()), fieldValue));
                        }
                    } catch (Exception e) {
                        log.error("判断不为空添加相等的查询条件时，出现异常。", e);
                    }
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
