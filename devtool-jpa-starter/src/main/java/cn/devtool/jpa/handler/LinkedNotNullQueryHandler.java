package cn.devtool.jpa.handler;

import cn.devtool.core.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;
import java.util.List;

/**
 * LinkedNotNullQuery注解业务处理类
 *
 * @author chentiefeng
 * @created 2020-10-12 12:54
 */
@Slf4j
public class LinkedNotNullQueryHandler implements IAnnotationHandler {

    private static final String LINKED_FIELD_SUFFIX = "2";

    @Override
    public  void handle(Field field, Object obj, List<Predicate> predicates, Root root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if (field.getName().endsWith(LINKED_FIELD_SUFFIX)) {
            return;
        }
        try {
            field.setAccessible(true);
            String fieldValue = (String) field.get(obj);
            Field field2 = obj.getClass().getDeclaredField(field.getName() + LINKED_FIELD_SUFFIX);
            field2.setAccessible(true);
            String field2Value = (String) field2.get(obj);
            if (StringUtils.nonBlank(fieldValue)) {
                //判断第二条件不为空
                if (StringUtils.nonBlank(field2Value)) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(field.getName()), fieldValue));
                } else {
                    predicates.add(criteriaBuilder.equal(root.get(field.getName()), fieldValue));
                }
            }
            if (StringUtils.nonBlank(field2Value)) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(field.getName()), field2Value));
            }
        } catch (Exception e) {
            log.error("判断两个关联字段不为空添加查询条件时，出现异常。", e);
        }
    }
}
