package cn.devtool.jpa.handler;

import cn.devtool.core.util.ReflectUtils;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;
import java.util.List;

/**
 * NotNullQuery注解处理类
 *
 * @author chentiefeng
 * @created 2020-10-12 12:49
 */
@Slf4j
public class NotNullInHandler implements IAnnotationHandler {

    @Override
    public  void handle(Field field, Object obj, List<Predicate> predicates, Root root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        try {
            List<String> fieldValue = (List<String>) ReflectUtils.getField(field, obj);
            if (fieldValue != null && fieldValue.size() > 0) {
                predicates.add(root.get(field.getName()).in(fieldValue));
            }
        } catch (Exception e) {
            log.error("判断不为空添加多个值的in查询条件（多个值之间用英文逗号分隔）时，出现异常。", e);
        }
    }
}
