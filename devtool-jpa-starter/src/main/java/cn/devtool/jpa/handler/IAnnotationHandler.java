package cn.devtool.jpa.handler;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;
import java.util.List;

/**
 * 注解处理类接口
 *
 * @author chentiefeng
 * @created 2020-10-12 13:01
 */
public interface IAnnotationHandler {
    /**
     * 处理流程
     * @param field 字段
     * @param obj 对象
     * @param predicates Predicate集合
     * @param query query
     * @param root root
     * @param criteriaBuilder CriteriaBuilder
     */
    void handle(Field field, Object obj, List<Predicate> predicates, Root root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder);
}
