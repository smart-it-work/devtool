package cn.devtool.jpa.handler;

import cn.devtool.core.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;
import java.util.List;

/**
 * NotNullNotEqual注解处理类
 *
 * @author chentiefeng
 * @created 2020-10-12 12:49
 */
@Slf4j
public class NotNullNotEqualHandler implements IAnnotationHandler {

    @Override
    public  void handle(Field field, Object obj, List<Predicate> predicates, Root root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        try {
            field.setAccessible(true);
            String fieldValue = (String) field.get(obj);
            if (StringUtils.nonBlank(fieldValue)) {
                predicates.add(criteriaBuilder.notEqual(root.get(field.getName()), fieldValue));
            }
        } catch (Exception e) {
            log.error("判断不为空添加相等的查询条件时，出现异常。", e);
        }
    }

}
