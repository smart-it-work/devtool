package cn.devtool.jpa.model;

import lombok.Data;

import java.util.List;

/**
 * 分页对象
 *
 * @author chentiefeng
 * @created 2021/7/14 10:51
 */
@Data
public class PageDto<T> {
    /**
     * 分页数据
     */
    private List<T> dataList;

    /**
     * 总条数
     */
    private long total;
    /**
     * 总页数
     */
    private long totalPages;

    /**
     * 当前页码
     */
    private long pageCurrent;
    /**
     * 每页大小
     */
    private long pageSize;
}
