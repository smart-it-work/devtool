package cn.devtool.jpa.annotation;



import cn.devtool.jpa.handler.IAnnotationHandler;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Jpa辅助注解
 *
 * @author chentiefeng
 * @created 2020-10-12 13:15
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JpaAnnotation {
    Class<? extends IAnnotationHandler> handler();
}
