package cn.devtool.jpa.annotation;



import cn.devtool.jpa.handler.NotNullEqualHandler;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 判断不为空添加相等的查询条件
 *
 * @author chentiefeng
 * @created 2020-10-12 09:56
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@JpaAnnotation(handler = NotNullEqualHandler.class)
public @interface NotNullEqual {
}
