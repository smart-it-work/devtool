package cn.devtool.jpa.annotation;



import cn.devtool.jpa.handler.NotNullNotInHandler;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 不为空时，添加not in条件
 *
 * @author chentiefeng
 * @created 2020-10-12 09:56
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@JpaAnnotation(handler = NotNullNotInHandler.class)
public @interface NotNullNotIn {
}
