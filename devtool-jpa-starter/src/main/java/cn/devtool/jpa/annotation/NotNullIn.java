package cn.devtool.jpa.annotation;


import cn.devtool.jpa.handler.NotNullInHandler;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 判断不为空添加多个值的in查询条件（多个值之间用英文逗号分隔）
 *
 * @author chentiefeng
 * @created 2020-10-12 09:56
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@JpaAnnotation(handler = NotNullInHandler.class)
public @interface NotNullIn {
}
