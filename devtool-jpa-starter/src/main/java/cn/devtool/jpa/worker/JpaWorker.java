package cn.devtool.jpa.worker;

import cn.devtool.woker.group.IGroupWorker;
import cn.devtool.woker.group.model.GroupWorkerParam;
import cn.devtool.woker.group.model.GroupWorkerResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;

/**
 * jpa分组Worker
 *
 * @author chentiefeng
 * @created 2021/7/14 10:19
 */
public class JpaWorker<T> implements IGroupWorker<List<T>> {
    /**
     * jpa repository
     */
    private JpaSpecificationExecutor<T> repository;

    /**
     * 查询条件
     */
    private Specification<T> specification;
    /**
     * 单个元素的消费者
     */
    private Consumer<T> itemConsumer;

    public JpaWorker(JpaSpecificationExecutor<T> repository, Specification<T> specification, Consumer<T> itemConsumer) {
        this.repository = repository;
        this.specification = specification;
        this.itemConsumer = itemConsumer;
    }


    @Override
    public GroupWorkerResult doProcess(GroupWorkerParam<List<T>> listGroupWorkerParam) {
        List<T> dataList = listGroupWorkerParam.getData();
        if (!CollectionUtils.isEmpty(dataList)) {
            dataList.forEach(itemConsumer);
        }
        return null;
    }

    @Override
    public long getTotal() {
        return repository.count(specification);
    }

    @Override
    public BiFunction<Integer, Integer, List<T>> getWorkList() {
        return (Integer start, Integer limit) -> {
            int currentPage = BigDecimal.valueOf(Math.ceil(start * 1.0 / limit)).intValue() + 1;
            Pageable pageable = new PageRequest(currentPage, limit);
            Page pageData = repository.findAll(specification, pageable);
            return pageData.getContent();
        };
    }
}
