package cn.devtool.jpa.test.util;

import cn.devtool.jpa.util.SpecificationUtils;
import org.springframework.data.jpa.domain.Specification;

/**
 * SpecificationUtils 测试类
 * @author chentiefeng
 * @created 2021/7/14 10:40
 */
public class SpecificationUtilsTest {
    public static void main(String[] args) {
        String username = "admin";
        // 测试创建 Specification
        Specification<Object> specification = SpecificationUtils.createSpecification((root, criteriaBuilder) -> criteriaBuilder.equal(root.get("name"), username),
                (root, criteriaBuilder) -> criteriaBuilder.equal(root.get("email"), username),
                (root, criteriaBuilder) -> criteriaBuilder.equal(root.get("phone"), username));

    }
}
