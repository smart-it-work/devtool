package cn.devtool.mybatis.model;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 数据日志模板
 *
 * @author chentiefeng
 * @created 2021/7/7 11:01
 */
@Data
public class SqlLogModel {
    /**
     * 本次的请求id
     */
    private String requestId;
    /**
     * 请求时间
     */
    private LocalDateTime requestTime;
    /**
     * 用户
     */
    private String username;

    /**
     * 数据库Key
     */
    private String datasourceKey;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;
    /**
     * 执行sql，参数用?
     */
    private String execSql;
    /**
     * 参数
     */
    private String params;

    /**
     * 完整sql，已经替换参数
     */
    private String sql;
    /**
     * 结束时间
     */
    private LocalDateTime endTime;
}
