package cn.devtool.mybatis.plugin;

import cn.devtool.core.util.StringUtils;
import cn.devtool.mybatis.model.SqlLogModel;
import cn.devtool.mybatis.repository.ISqlLogRepository;
import cn.devtool.mybatis.utils.MybatisPluginUtils;
import cn.devtool.spring.request.context.RequestInfoContextHolder;
import cn.devtool.spring.request.model.RequestInfoContext;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Properties;

/**
 * Mybatis调试日志插件
 * @author chentiefeng
 * @created 2021/7/7 11:03
 */
@Intercepts({@Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})})
public class MybatisSqlLogPlugin implements Interceptor {
    /**
     * 自动注入存储
     */
    @Autowired
    private ISqlLogRepository sqlLogRepository;
    /**
     * 方法拦截
     * @param invocation invocation
     * @return Object
     * @throws Throwable Throwable
     */
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        RequestInfoContext requestInfoContext = RequestInfoContextHolder.getContext();
        if (requestInfoContext != null && requestInfoContext.isTrack()) {
            //通过StatementHandler获取执行的sql
            StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
            BoundSql boundSql = statementHandler.getBoundSql();

            SqlLogModel sqlLogModel = createInitModel(requestInfoContext);
            // 执行的sql语句
            String sql = StringUtils.formatSql(boundSql.getSql());
            // 参数集合
            List<Object> params = MybatisPluginUtils.getParameters(boundSql);
            sqlLogModel.setExecSql(sql);
            sqlLogModel.setParams(StringUtils.join(params, ","));
            sqlLogModel.setSql(MybatisPluginUtils.sqlFormatParameters(sql, params));
            sqlLogModel.setStartTime(LocalDateTime.now());
            Object returnObj = invocation.proceed();
            sqlLogModel.setEndTime(LocalDateTime.now());
            if (sqlLogRepository != null) {
                // 保存到sql执行日志
                sqlLogRepository.save(sqlLogModel);
            }
            return returnObj;
        } else {
            return invocation.proceed();
        }
    }

    /**
     * 初始化创建 SqlLogModel
     * @param requestInfoContext  RequestInfoContext
     * @return SqlLogModel
     */
    private SqlLogModel createInitModel(RequestInfoContext requestInfoContext) {
        SqlLogModel sqlLogModel = new SqlLogModel();
        sqlLogModel.setRequestId(requestInfoContext.getRequestId());
        sqlLogModel.setRequestTime(requestInfoContext.getRequestTime());
        sqlLogModel.setUsername(requestInfoContext.getUsername());
        return  sqlLogModel;
    }
    /**
     * 它是一个生成动态代理对象的方法
     * @param target target
     * @return Object
     */
    @Override
    public Object plugin(Object target) {
        //使用Plugin的wrap方法生成代理对象
        return Plugin.wrap(target, this);
    }

    /**
     * 允许你在使用插件的时候设置参数值, 常用来获取设置的阈值等参数
     * @param properties properties
     */
    @Override
    public void setProperties(Properties properties) {
        // 可以在这里设置或者获取参数值
    }
}
