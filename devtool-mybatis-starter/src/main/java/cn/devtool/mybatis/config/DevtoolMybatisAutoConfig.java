package cn.devtool.mybatis.config;

import cn.devtool.mybatis.plugin.MybatisSqlLogPlugin;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * devtool-mybatis自动配置类
 *
 * @author chentiefeng
 * @created 2021/7/8 14:44
 */
@Configuration
public class DevtoolMybatisAutoConfig implements InitializingBean {
    @Autowired
    private List<SqlSessionFactory> sqlSessionFactoryList;

    @Override
    public void afterPropertiesSet() {
        Interceptor interceptor = mybatisDebugSqlLogPlugin();
        for (SqlSessionFactory sqlSessionFactory : sqlSessionFactoryList) {
            org.apache.ibatis.session.Configuration configuration = sqlSessionFactory.getConfiguration();
            if (!configuration.getInterceptors().contains(interceptor)) {
                configuration.addInterceptor(interceptor);
            }
        }
    }

    @Bean
    public MybatisSqlLogPlugin mybatisDebugSqlLogPlugin() {
        return new MybatisSqlLogPlugin();
    }
}
