package cn.devtool.mybatis.utils;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.ParameterMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Mybatis插件工具类
 *
 * @author chentiefeng
 * @created 2021/7/8 15:41
 */
public class MybatisPluginUtils {
    private MybatisPluginUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 获取 Parameter值集合
     * @param boundSql BoundSql对象
     * @return Parameter值集合
     */
    public static List<Object> getParameters(BoundSql boundSql) {
        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappings();
        List<Object> params = new ArrayList<>(parameterMappingList.size());
        for(ParameterMapping tmpParameterMapping: parameterMappingList){
            // 是否含有分页项
            if ("First_PageHelper".equals(tmpParameterMapping.getProperty())
                    || "Second_PageHelper".equals(tmpParameterMapping.getProperty())) {
                Object parameterObj = boundSql.getParameterObject();
                if (Objects.nonNull(parameterObj) && parameterObj instanceof Map) {
                    params.add(((Map<?,?>)parameterObj).get(tmpParameterMapping.getProperty()));
                    continue;
                }
            }
            params.add(boundSql.getAdditionalParameter(tmpParameterMapping.getProperty()));
        }
        return params;
    }

    /**
     * sql语句填入参数后的sql语句
     * @param sql 带?占位符的sql语句
     * @param params 参数集合
     * @return sql填入参数后的语句
     */
    public static String sqlFormatParameters(String sql, List<Object> params) {
        return String.format(sql.replace("?","%s"), params.stream().map(tmpParam -> {
            if (tmpParam == null) {
                return "NULL";
            }
            if (tmpParam instanceof String) {
                return "'" + tmpParam + "'";
            }
            return tmpParam;
        }).toArray());
    }
}
