package cn.devtool.mybatis.repository;


import cn.devtool.core.util.FileUtils;
import cn.devtool.core.util.JsonUtils;
import cn.devtool.core.util.StringUtils;
import cn.devtool.mybatis.model.SqlLogModel;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 文件日志保存
 *
 * @author chentiefeng
 * @created 2021/7/7 11:29
 */
@Slf4j
public class FileSqlLogRepository implements ISqlLogRepository{

    /**
     * 保存路径
     */
    private String savePath;
    /**
     * 保存文件名
     */
    private String saveFileName;

    /**
     * 构造方法
     * @param savePath sql日志保存路径
     * @param saveFileName 保存文件名
     */
    public FileSqlLogRepository(String savePath, String saveFileName) {
        this.savePath = savePath;
        this.saveFileName = saveFileName;
    }

    @Override
    public void save(SqlLogModel sqlLogModel) {
        // 设置默认存放路径为系统的tmp路径
        savePath = StringUtils.getOrDefault(savePath, System.getProperty("java.io.tmpdir"));

        if (StringUtils.isBlank(saveFileName)) {
            // 设置默认文件名为 用户名-yyyyMMdd-mybatis-sql-log.json
            String username = sqlLogModel.getUsername();
            String timeStampStr = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
            saveFileName = username +"_" + timeStampStr +"-mybatis-sql-log.json";
        }
        try {
            FileUtils.appendToFile(Paths.get(savePath, saveFileName), JsonUtils.toJson(sqlLogModel)+"\n");
        } catch (IOException e) {
            log.error("mybatis-sql日志json文件保存出现异常.", e);
        }
    }
}
