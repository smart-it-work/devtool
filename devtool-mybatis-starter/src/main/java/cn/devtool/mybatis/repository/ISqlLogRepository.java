package cn.devtool.mybatis.repository;


import cn.devtool.mybatis.model.SqlLogModel;

/**
 * sql日志 存储
 * @author chentiefeng
 * @created 2021/7/7 11:15
 */
public interface ISqlLogRepository {
    /**
     * 保存日志
     * @param sqlLogModel sql日志模型
     */
    void save(SqlLogModel sqlLogModel);
}
