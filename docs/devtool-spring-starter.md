# devtool-spring-starter

## 简介

`devtool-spring-starter`是针对Spring中的一些常用操作的封装，可以帮助我们在开发过程中，开箱即用一些功能。

## 使用

``` xml
        <dependency>
            <groupId>cn.devtool</groupId>
            <artifactId>devtool-spring-starter</artifactId>
            <version>1.0.1</version>
        </dependency>
```

## 功能介绍

### 1、封装Controller的返回结果

​		`devtool-spring-starter`模块提供了开箱即用的Controller结果封装功能，通过在**Controller上**或者在**Controller的方法上**添加`@EnableResponseResult`，程序会自动将返回结果封装为`cn.devtool.spring.response.model.ResponseResult`对象。

**注意**：如果返回值为`String`类型的Controller，即使添加了`@EnableResponseResult`，也不会自动封装。

#### 使用示例

##### Controller代码

``` java
@RestController
@RequestMapping("test")
public class TestController {

    @EnableResponseResult
    @GetMapping("/user")
    public User test() {
        User user = new User();
        user.setUsername("chentiefeng");
        user.setAge(10);
        return user;
    }
}
@Data
class User {
    private String username;
    private int age;
}
```

##### 响应结果

访问http://localhost:8080/test/user ，可以得到如下结果：

```json
{
    "success": true,
    "code": 200,
    "message": "success",
    "data": {
        "username": "chentiefeng",
        "age": 10
    }
}
```

### 2、记录FeignClient的日志

`devtool-spring-starter`模块了一个切片类，用于对`FeignClient`发出的请求进行拦截，默认情况下，只记录FeignClient出现异常时的日志。

用户可以通过实现`FeignClientLogMapper`接口，实现对Feign调用日志的保存操作，日志的记录对象`FeignClientLog`的字段信息如下：

- `id`：本次请求的唯一标识，使用的uuid
- `groupId`：批次号id，如果一个request请求调用了多个FeignClient，则多次FeignClient记录的该值相等，该值都等于request的requestId；
- `user`：操作用户
- `clientServiceName`：`@FeignClient`中配置的name值
- `clientServiceUrl`：`@FeignClient`中配置的url值
- `clientServicePath`：`@FeignClient`中配置的path值
- `clientClass`：注解`@FeignClient`的类调用的方法信息
- `requestUrl`：调用的url地址，是`@GetMapping`、`@PostMapping`等注解中的value值
- `requestMethod`：请求方法，如：GET，POST
- `requestParams`：请求参数
- `startTime`：请求开始时间
- `endTime`：请求结束时间
- `success`：请求是否成功
- `message`：请求失败的提示信息
- `detail`：请求失败的详细提示信息

**注意**：自定义的`FeignClientLogMapper`实现类需要注入到Spring容器中。

示例：

```java
@Component
public class FeignClientLogMapper implements IFeignClientLogMapper {
    @Override
    public void save(FeignClientLog clientLog) {
        System.out.println(CheckUtils.checkValueException(() -> JsonUtils.toJson(clientLog), null));
    }
}
```

**注释**：`CheckUtils`是一个工具类，它的`checkValueException`方法，用于在参数1中的值没有出现异常时，返回参数1的值；参数1如果出现异常，返回参数2的值，源码如下：

``` java
    /**
     * 检查带有exception的值，如果出现Exception，返回 defaultValue
     * @param function 获取数值的带有Exception的函数
     * @param defaultValue 默认值
     * @param <R> 结果类型
     * @return 结果值
     */
    public static <R> R checkValueException(CheckFunction<R> function, R defaultValue) {
        try {
            return function.apply();
        } catch (Throwable e) {
            return defaultValue;
        }
    }
```

### 3、SpringContextUtils

`devtool-spring-starter`提供了一个工具类SpringContextUtils，SpringContextUtils是一个封装了SpringContext的工具类，使用该工具类可以方便的获取SpringContext容器中的Bean，如：

```java
UserService userService = SpringContextUtils.getBean(UserService.class);
```

### 4、RequestInfoContext

​		`devtool-spring-starter`提供了一个`RequestInfoContextHolder`，用于存放一些request的信息，详细的request信息可以参考`RequestInfoContext`，默认使用的是`DefaultRequestInfoContextBuilder`类构建的`RequestInfoContext`的内容。

​		如果用户想要通过`RequestInfoContextHolder`存放更多信息，可以继承`DefaultRequestInfoContextBuilder`类或者实现`IRequestInfoContextBuilder`接口，自定义`RequestInfoContext`的构造方法，来实现更多信息的存放。

**注意**：自定义的`IRequestInfoContextBuilder`实现类需要注入到Spring容器中。

使用示例：

``` java
@Component
public class AuthRequestInfoContextBuilder extends DefaultRequestInfoContextBuilder {

    @Autowired
    private AuthServiceClient authServiceClient;

    public AuthRequestInfoContextBuilder(String tokenKey) {
        super(tokenKey);
    }

    @Override
    public RequestInfoContext build(HttpServletRequest request) {
        RequestInfoContext context =  super.build(request);
        context.setTrack(true);
        String token = request.getHeader(tokenKey);
        context.setToken(token);
        if (!StringUtils.isEmpty(token)) {
            ResponseResult<UserBO> responseResult = authServiceClient.getTokenValue(token.trim());
            if (responseResult.isSuccess() && Objects.nonNull(responseResult.getData())) {
                UserBO user = responseResult.getData();
                context.setUsername(user.getUsername());
                context.setUserId(user.getUserId());
                Map<String, Object> additionalData = new HashMap<>();
                additionalData.put("email", user.getEmail());
                additionalData.put("phone", user.getPhone());
                context.setAdditionalData(additionalData);
            }
        }
        return context;
    }
}
```

使用RequestInfoContextHolder示例：

```java
String user = RequestInfoContextHolder.getContext().getUsername();
```

### 5、请求日志记录

`devtool-spring-starter`提供了`IRequestLogMapper`接口，如果用户实现了`IRequestLogMapper`接口，并注册进了Spring容器中，则会自动开启Request日志的记录，用户可以在`IRequestLogMapper`接口的实现类中，进行Request日志信息的保存。

Request日志信息`RequestLog`字段如下：

- `id`：本次请求的唯一主键，在程序中可以通过`RequestInfoContextHolder.getContext().getRequestId()`获取到该值，通过在日志或者数据中使用该值，可以进行单次请求的链路追踪，如使用该值追踪FeignClient的调用日志；
- `url`：请求的url地址
- `user`：请求的用户
- `requestTime`：请求的开始时间
- `requestParams`：请求参数
- `requestBody`：请求报文体，如：`application/json;charset=UTF-8`类型的请求，就可以在这里看到请求内容

示例：

```java
@Component
public class RequestLogMapper implements IRequestLogMapper {
    @Override
    public void save(RequestLog requestUrlLog) {
        System.out.println(CheckUtils.checkValueException(() -> JsonUtils.toJson(requestUrlLog), null));
    }
}
```

**注释**：`CheckUtils`是一个工具类，它的`checkValueException`方法，用于在参数1中的值没有出现异常时，返回参数1的值；参数1如果出现异常，返回参数2的值，源码如下：

``` java
    /**
     * 检查带有exception的值，如果出现Exception，返回 defaultValue
     * @param function 获取数值的带有Exception的函数
     * @param defaultValue 默认值
     * @param <R> 结果类型
     * @return 结果值
     */
    public static <R> R checkValueException(CheckFunction<R> function, R defaultValue) {
        try {
            return function.apply();
        } catch (Throwable e) {
            return defaultValue;
        }
    }
```





