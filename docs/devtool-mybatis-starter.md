Mybatis使用：

1、添加依赖

``` xml
		<dependency>
			<groupId>cn.devtool</groupId>
			<artifactId>devtool-mybatis-starter</artifactId>
			<version>1.0.1</version>
		</dependency>
```

2、创建DefaultRequestInfoContextBuilder的继承类：

``` java
public class RedisRequestInfoContextBuilder extends DefaultRequestInfoContextBuilder {
    @Autowired
    private RedisUtil redisUtil;

    public RedisRequestInfoContextBuilder(String tokenKey) {
        super(tokenKey);
    }

    @Override
    public RequestInfoContext build(HttpServletRequest request) {
        RequestInfoContext context =  super.build(request);
        context.setTrack(true);
        String token = request.getHeader(tokenKey);
        UserRoles userInfo = redisUtil.getUserInfo(token);
        context.setUsername(userInfo.getName());
        return context;
    }
}
```

3、配置

``` java
@Configuration
public class MybatisSqlLogConfig {
    @Bean
    public RedisRequestInfoContextBuilder requestInfoContextBuilder() {
        return new RedisRequestInfoContextBuilder("X-Token");
    }

    @Bean
    public FileSqlLogRepository sqlLogRepository() {
        return new FileSqlLogRepository("C:\\Users\\imche\\Desktop\\logs",null);
    }
}
```













































