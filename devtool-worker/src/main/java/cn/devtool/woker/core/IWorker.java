package cn.devtool.woker.core;

/**
 * Worker接口，用于处理流程
 *
 * @author chentiefeng
 * @created 2021/7/12 12:04
 */
public interface IWorker<T, R> {
    /**
     * 进行工作处理
     * @param t 工作入参
     * @return 工作结果
     */
    R doProcess(T t);
}
