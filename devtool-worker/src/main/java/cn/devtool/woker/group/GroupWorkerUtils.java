package cn.devtool.woker.group;

import cn.devtool.woker.group.model.GroupWorkerParam;
import lombok.extern.slf4j.Slf4j;


/**
 * 分组Worker工具类
 *
 * @author chentiefeng
 * @created 2021/7/12 14:01
 */
@Slf4j
public class GroupWorkerUtils {
    private GroupWorkerUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 分组工作
     * @param start 开始index
     * @param limit 每页条数
     * @param worker IGroupWorker
     * @param <R> 返回值泛型
     */
    public static <R> void group(int start, int limit, IGroupWorker<R> worker) {
        long total = worker.getTotal();
        GroupWorkerParam<R> param = new GroupWorkerParam<R>(start, limit, total);
        param.setCurrentPage(1);
        long startTime = System.currentTimeMillis();
        log.debug("begin...");
        while (param.getCurrentPage() <= param.getTotalPage()) {
            log.debug("第{}组[begin]...", param.getCurrentPage());
            param.setData(worker.getWorkList().apply(param.getStart(), param.getLimit()));
            worker.doProcess(param);
            long end = param.getStart() + limit - 1;
            if (end > total) {
                end = total;
            }
            log.debug("第{}组[end]: 已用时:{}, 完成：{}% [{} ~ {}]", param.getCurrentPage(), (System.currentTimeMillis() - startTime), (param.getCurrentPage()*1.0/param.getTotalPage()*100), param.getStart(), end);
            param.setStart(param.getStart() + limit);
            param.setCurrentPage(param.getCurrentPage() + 1);
        }
        long endTime = System.currentTimeMillis();
        log.debug("end... useTime:{}", (endTime-startTime));
    }
}
