package cn.devtool.woker.group.worker;

import cn.devtool.woker.group.IGroupWorker;
import cn.devtool.woker.group.model.GroupWorkerParam;
import cn.devtool.woker.group.model.GroupWorkerResult;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Csv文件分组读取
 *
 * @author chentiefeng
 * @created 2021/6/21 18:12
 */
@Slf4j
public class CsvWorker implements IGroupWorker<Stream<String>> {
    /**
     * csv文件路径
     */
    private final Path filePath;
    /**
     * 行消费者
     */
    private final BiConsumer<Integer, String[]> lineConsumer;
    /**
     * 列过滤器
     */
    private Predicate<Integer> filterColumnPredicate;
    public CsvWorker(Path filePath, BiConsumer<Integer, String[]> lineConsumer) {
        this.filePath = filePath;
        this.lineConsumer = lineConsumer;
    }

    public CsvWorker(Path filePath, Predicate<Integer> filterColumnPredicate, BiConsumer<Integer, String[]> lineConsumer) {
        this.filePath = filePath;
        this.filterColumnPredicate = filterColumnPredicate;
        this.lineConsumer = lineConsumer;
    }


    @Override
    public long getTotal() {
        try(Stream<String> lineStream = Files.lines(filePath)) {
            return lineStream.count();
        } catch (IOException e) {
            log.error("查询Total出现异常！", e);
            e.printStackTrace();
        }
       return 0L;
    }

    @Override
    public BiFunction<Integer, Integer, Stream<String>> getWorkList() {
        return (Integer start, Integer limit) -> {
            try {
                return Files.lines(filePath).skip(start-1).limit(limit);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Stream.of("");
        };
    }

    @Override
    public GroupWorkerResult doProcess(GroupWorkerParam<Stream<String>> streamGroupWorkerParam) {
        AtomicInteger lineNumber = new AtomicInteger(streamGroupWorkerParam.getStart());
        streamGroupWorkerParam.getData().forEach(cell -> {
            Pattern cellPattern = Pattern.compile("(\"[^\"]*(\"{2})*[^\"]*\")*[^,]*,");
            Matcher cellMatcher = cellPattern.matcher(cell);
            List<String> rowValue = new ArrayList<>(cellMatcher.groupCount());
            int columnIndex = 0;
            //读取每个单元格
            while (cellMatcher.find()) {
                boolean filterFlag = filterColumnPredicate==null || (filterColumnPredicate!=null && filterColumnPredicate.test(columnIndex));
                if (filterFlag) {
                    rowValue.add(cellMatcher.group().replaceAll("(?sm)\"?([^\"]*(\"{2})*[^\"]*)\"?.*,", "$1")
                            .replaceAll("(?sm)(\"(\"))", "$2"));
                }
                columnIndex ++;
            }
            lineConsumer.accept(lineNumber.get(), rowValue.toArray(rowValue.toArray(new String[0])));
            lineNumber.getAndIncrement();
        });
        return null;
    }
}
