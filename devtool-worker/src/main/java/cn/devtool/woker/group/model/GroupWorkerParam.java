package cn.devtool.woker.group.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 分组工作参数
 *
 * @author chentiefeng
 * @created 2021/7/12 13:54
 */
public class GroupWorkerParam<T> {
    /**
     * 开始index
     */
    private int start;

    /**
     * 总条数
     */
    private long total;

    /**
     * 每页条数
     */
    private int limit;

    /**
     * 总页数
     */
    private int totalPage;
    /**
     * 当前页码
     */
    private int currentPage;

    private T data;

    public GroupWorkerParam(int start, int limit, long total) {
        this.start = start;
        this.limit = limit;
        this.total = total;
    }

    public int getTotalPage() {
        return BigDecimal.valueOf(Math.ceil((total-start) * 1.0 / limit)).intValue();
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getStart() {
        return start;
    }

    public long getTotal() {
        return total;
    }

    public int getLimit() {
        return limit;
    }
}
