package cn.devtool.woker.group;

import cn.devtool.woker.core.IWorker;
import cn.devtool.woker.group.model.GroupWorkerParam;
import cn.devtool.woker.group.model.GroupWorkerResult;

import java.util.function.BiFunction;

/**
 * 分组Worker接口
 *
 * @author chentiefeng
 * @created 2021/7/12 13:53
 */
public interface IGroupWorker<T> extends IWorker<GroupWorkerParam<T>, GroupWorkerResult> {
    /**
     * 总条数函数
     * @return 总条数
     */
    long getTotal();

    /**
     * 获取分组后的工作数据
     * @return 分组后的工作数据 /start, limit, 分组查询的集合对象/
     */
    BiFunction<Integer, Integer, T> getWorkList();
}
