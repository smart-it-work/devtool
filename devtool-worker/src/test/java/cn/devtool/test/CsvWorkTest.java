package cn.devtool.test;

import cn.devtool.woker.group.GroupWorkerUtils;
import cn.devtool.woker.group.worker.CsvWorker;

import java.nio.file.Paths;

/**
 * @author chentiefeng
 * @created 2021/7/12 14:43
 */
public class CsvWorkTest {
    public static void main(String[] args) {
        CsvWorker worker = new CsvWorker(Paths.get("C:\\Users\\imche\\Desktop\\upload","_rdl_so_raw_all_combine_act__202106171101.csv"),
                index -> index == 0, (index,item) -> {
            System.out.println("line:"+ index +" ***" + String.join(",", item));
        });
        GroupWorkerUtils.group(1, 100, worker);
    }
}
