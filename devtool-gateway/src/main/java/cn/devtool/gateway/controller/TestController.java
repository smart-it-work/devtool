package cn.devtool.gateway.controller;

import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试Controller
 *
 * @author chentiefeng
 * @created 2021/7/27 11:35
 */
@RefreshScope
@RestController
public class TestController {
    @GetMapping
    public String test() {
        return "success";
    }
}
