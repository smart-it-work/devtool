package cn.devtool.spring.enums;

import lombok.Getter;

/**
 * Xass返回数据编码枚举
 *
 * @author chentiefeng
 * @created 2020-10-21 15:12
 */
@Getter
public enum ResultCodeType {
    /**
     * 成功
     */
    OK(200, "success"),
    /**
     * 服务出现异常
     */
    ERROR(500, "Internal server error, please contact administrator.");
    private int code;
    private String message;
    ResultCodeType(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
