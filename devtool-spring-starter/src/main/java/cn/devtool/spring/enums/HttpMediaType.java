package cn.devtool.spring.enums;

import org.springframework.http.MediaType;

/**
 * Http Contenttype的值枚举
 *
 * @author chentiefeng
 * @created 2021/7/12 16:54
 */
public class HttpMediaType extends MediaType {
    /**
     * Excel
     */
    public static final MediaType APPLICATION_EXCEL = valueOf("application/vnd.ms-excel");
    public static final MediaType APPLICATION_EXCEL_VALUE = valueOf("application/vnd.ms-excel");

    /**
     * html
     */
    public static final MediaType TEXT_HTML_UTF8 = valueOf("text/html;charset=utf-8");
    public static final MediaType TEXT_HTML_UTF8_VALUE = valueOf("text/html;charset=utf-8");

    public HttpMediaType(String type) {
        super(type);
    }
}
