package cn.devtool.spring.condition;

import cn.devtool.core.util.ClassUtils;
import org.springframework.boot.autoconfigure.condition.ConditionMessage;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 当Feign存在的条件
 *
 * @author chentiefeng
 * @created 2021/7/15 19:21
 */
public class OnFeignCondition extends SpringBootCondition {
    /**
     * Feign客户端类名
     */
    private static final String FEIGN_CLIENT_CLASS_NAME = "org.springframework.cloud.openfeign.FeignClient";
    @Override
    public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
        ConditionMessage.Builder message = ConditionMessage.forCondition("Feign Condition");
        return ClassUtils.checkExist(FEIGN_CLIENT_CLASS_NAME) ?
                    ConditionOutcome.match(message.found("org.springframework.cloud.openfeign.FeignClient Class").atAll())
                    : ConditionOutcome.noMatch(message.didNotFind("org.springframework.cloud.openfeign.FeignClient Class").atAll());
    }

}
