package cn.devtool.spring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * 跨域配置
 *
 * @author chentiefeng
 * @created 2021/7/17 19:21
 */
@Configuration
public class CorsConfiguration extends WebMvcConfigurationSupport {

    /**
     * 为了解决跨域问题,所以重写addCorsMappings方法
     * @param registry CorsRegistry
     */
    @Override
    protected void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("GET", "HEAD", "POST","PUT", "DELETE", "OPTIONS")
                .allowedHeaders("*")
                .exposedHeaders("access-control-allow-headers",
                        "access-control-allow-methods",
                        "access-control-allow-origin",
                        "access-control-max-age",
                        "X-Frame-Options")
                .allowCredentials(false).maxAge(18000L);
        super.addCorsMappings(registry);
    }
}
