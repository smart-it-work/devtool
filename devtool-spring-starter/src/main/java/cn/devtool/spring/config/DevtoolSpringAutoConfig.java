package cn.devtool.spring.config;

import cn.devtool.spring.condition.ConditionalOnFeign;
import cn.devtool.spring.log.clientlog.FeignClientLogAspect;
import cn.devtool.spring.log.datalog.DataChangeLogService;
import cn.devtool.spring.log.datalog.IDataChangeLogMapper;
import cn.devtool.spring.log.urllog.IRequestLogMapper;
import cn.devtool.spring.log.urllog.RequestLogFilter;
import cn.devtool.spring.request.context.RequestInfoContextListener;
import cn.devtool.spring.util.SpringContextUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * devtool-spring自动配置类
 *
 * @author chentiefeng
 * @created 2021/7/8 14:17
 */
@Configuration
@ComponentScan("cn.devtool.spring.response.aspect")
public class DevtoolSpringAutoConfig {


    @ConditionalOnFeign
    @Bean
    public FeignClientLogAspect clientExceptionAspect() {
        return new FeignClientLogAspect();
    }
    /**
     * 注册SpringContextUtils
     * @return SpringContextUtils
     */
    @ConditionalOnMissingBean
    @Bean
    public SpringContextUtils springContextUtils() {
        return new SpringContextUtils();
    }

    /**
     * 注册 RequestInfoContextListener
     * @return RequestInfoContextListener
     */
    @ConditionalOnMissingBean
    @Bean
    public RequestInfoContextListener requestInfoContextListener() {
        return new RequestInfoContextListener();
    }

    /**
     * 注册 Request请求日志过滤器
     * @return RequestLogFilter
     */
    @ConditionalOnBean(IRequestLogMapper.class)
    @Bean
    public RequestLogFilter urlLogFilter() {
        return new RequestLogFilter();
    }

    /**
     * 注册数据修改日志服务
     * @return 数据修改日志服务
     */
    @ConditionalOnBean(IDataChangeLogMapper.class)
    @Bean
    public DataChangeLogService dataLogService() {
        return new DataChangeLogService();
    }

}
