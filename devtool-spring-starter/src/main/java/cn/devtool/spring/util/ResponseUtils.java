package cn.devtool.spring.util;

import cn.devtool.spring.enums.HttpMediaType;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Objects;

/**
 * 下载工具类：主要用于封装下载的repsonse数据
 *
 * @author chentiefeng
 * @created 2021/7/12 16:30
 */
@Slf4j
public class ResponseUtils {
    private ResponseUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 封装下载的outputStream
     * @param response HttpServletResponse
     * @param mediaType mediaType
     * @param fileName 文件名
     * @return Response的outputStream
     * @throws IOException IO
     */
    public static OutputStream download(HttpServletResponse response, MediaType mediaType, String fileName) throws IOException {
        response.setContentType(mediaType.getType());
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        return response.getOutputStream();
    }

    /**
     * 重定向 本质上是修改status为302，修改 location 为 redirect的地址
     * @param request HttpServletResponse
     * @param response HttpServletResponse
     * @param redirectUrl 要redirect的地址
     */
    public static void redirect(HttpServletRequest request, HttpServletResponse response, String redirectUrl) {
        try {
            if (!redirectUrl.startsWith(HttpUtils.HTTP_PREFIX) && !redirectUrl.startsWith(HttpUtils.HTTPS_PREFIX)) {
                redirectUrl = request.getRequestURL().toString().replace(request.getServletPath(), "") + "/" + redirectUrl;
            }
            response.sendRedirect(redirectUrl);
        } catch (IOException e) {
            log.error("重定向到{}出现异常！", redirectUrl, e);
        }
    }

    /**
     * 输出html内容
     * @param response HttpServletResponse
     * @param htmlContent html内容
     */
    public static void writeHtml(HttpServletResponse response, String htmlContent) {
        //设置编码
        response.setContentType("text/html;charset=utf-8");
        try {
            //获取字符输出流
            PrintWriter pw = response.getWriter();
            //输出数据
            pw.write(htmlContent);
        } catch (IOException e) {
            log.error("输出Html内容{}出现异常！", htmlContent, e);
        }
    }

    /**
     * 输出图片
     * @param response HttpServletResponse
     * @param image 图片
     */
    public static void writeImage(HttpServletResponse response, RenderedImage image) {
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        try {
            ImageIO.write(image, "jpg", response.getOutputStream());
        } catch (IOException e) {
            log.error("输出图片内容出现异常！", e);
        }
    }

    /**
     * 输出图片
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param imagePath 图片路径，可以是网络路径，可以是本地路径
     */
    public static void writeImage(HttpServletRequest request, HttpServletResponse response, String imagePath) {
        writeImage(request, response, imagePath, false);
    }

    /**
     * 输出图片
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param imagePath 图片路径，可以是网络路径，可以是本地路径
     * @param downloadFlag 是否下载到本地
     */
    public static void writeImage(HttpServletRequest request, HttpServletResponse response, String imagePath, boolean downloadFlag) {
        writeFile(request, response, imagePath, downloadFlag);
    }

    public static void writeFile(HttpServletRequest request, HttpServletResponse response, String filePath, boolean downloadFlag) {
        response.setContentType(getMediaType(filePath).toString());
        if (downloadFlag) {
            //下载文件到本地,设置存放路径为系统的tmp路径
            String downloadFile = HttpUtils.downloadFile(filePath, System.getProperty("java.io.tmpdir"), String.valueOf(System.nanoTime()));
            if (Objects.nonNull(downloadFile) && !downloadFile.trim().isEmpty()) {
                filePath = downloadFile;
            }
        }
        File file = new File(filePath);
        if (file.exists()) {
            try (InputStream inputStream = new FileInputStream(file)) {
                IOUtils.copy(inputStream, response.getOutputStream());
                return;
            } catch (IOException e) {
                log.error("读取资源{}出现异常！", filePath, e);
            }
        }
        redirect(request, response, filePath);
    }

    /**
     * 通过文件名获取 MediaType
     * @param filename 文件名
     * @return MediaType
     */
    public static MediaType getMediaType(String filename) {
        if (filename.endsWith(".xlsx") || filename.endsWith(".xls")) {
            return HttpMediaType.APPLICATION_EXCEL;
        }
        if (filename.endsWith(".jpg")) {
            return MediaType.IMAGE_JPEG;
        }
        if (filename.endsWith(".png")) {
            return MediaType.IMAGE_PNG;
        }
        if (filename.endsWith(".gif")) {
            return MediaType.IMAGE_GIF;
        }
        if (filename.endsWith(".html")) {
            return HttpMediaType.TEXT_HTML_UTF8;
        }
        return MediaType.APPLICATION_OCTET_STREAM;
    }
}
