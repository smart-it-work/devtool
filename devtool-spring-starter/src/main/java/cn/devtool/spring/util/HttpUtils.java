package cn.devtool.spring.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.function.Function;

/**
 * Http工具类
 *
 * @author chentiefeng
 * @created 2021/7/13 11:09
 */
@Slf4j
public class HttpUtils {
    private HttpUtils() {
        throw new IllegalStateException("Utility class");
    }
    public static final String HTTP_PREFIX = "http://";
    public static final String HTTPS_PREFIX = "https://";

    /**
     * 执行get请求
     * @param url responseType
     * @param headers 请求头
     * @param responseType 响应类型
     * @param <T> 响应类型泛型
     * @return 响应 ResponseEntity
     */
    public static <T> ResponseEntity<T> get(String url, HttpHeaders headers, Class<T> responseType) {
        HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);
        RestTemplate restTemplate = HttpUtils.getRestTemplate();
        return restTemplate.exchange(url, HttpMethod.GET, requestEntity, responseType);
    }
    /**
     * 执行post请求
     * @param url 请求路径
     * @param param 请求参数
     * @param httpHeaders 请求头
     * @param responseType 响应类型
     * @param <P> 请求参数类型泛型
     * @param <T> 响应
     * @return 响应
     */
    public static  <T, P> ResponseEntity<T> post(String url, P param, HttpHeaders httpHeaders, Class<T> responseType) {
        RestTemplate restTemplate = getRestTemplate();
        HttpEntity<P> requestEntity = new HttpEntity<>(param, httpHeaders);
        return restTemplate.postForEntity(url, requestEntity, responseType);
    }
    public static  <T, P> ResponseEntity<T> post(String url, HttpHeaders httpHeaders, Class<T> responseType) {
        RestTemplate restTemplate = getRestTemplate();
        HttpEntity<P> requestEntity = new HttpEntity<>(null, httpHeaders);
        return restTemplate.postForEntity(url, requestEntity, responseType);
    }

    /**
     * 执行post请求
     * @param url 请求路径
     * @param paramsFun 参数转换方法
     * @param httpHeaders 请求头
     * @param params 参数
     * @param responseType 响应类型
     * @param <T> 请求参数类型泛型
     * @param <P> 响应
     * @return 响应
     */
    public static  <T, P> ResponseEntity<T> post(String url, Function<Map<String, Object>, P> paramsFun, HttpHeaders httpHeaders, Map<String, Object> params, Class<T> responseType) {
        RestTemplate restTemplate = getRestTemplate();
        HttpEntity<P> requestEntity = new HttpEntity<>(paramsFun.apply(params), httpHeaders);
        return restTemplate.postForEntity(url, requestEntity, responseType);
    }



    /**
     * 获取 RestTemplate
     * @return RestTemplate
     */
    public static RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    /**
     * 下载文件
     * @param url 文件的url路径
     * @param savePath 本地的保存路径
     * @param saveFile 保存文件名
     * @return 本地存储的绝对路径
     */
    public static String downloadFile(String url, String savePath, String saveFile) {
        File path = new File(savePath);
        if (!path.exists()) {
            boolean mkFlag = path.mkdirs();
            if (!mkFlag) {
                log.error("文件夹{}创建失败", savePath);
            }
        }
        if (!url.startsWith(HTTP_PREFIX) && !url.startsWith(HTTPS_PREFIX)) {
            log.error("文件{}下载失败, 文件路径不是http或者https开头!", url);
            return "";
        }
        RestTemplate restTemplate = HttpUtils.getRestTemplate();
        return restTemplate.execute(url, HttpMethod.GET,
                clientHttpRequest -> {},
                clientHttpResponse -> {
                    InputStream inputStream = clientHttpResponse.getBody();
                    File newFile = new File(savePath, saveFile);
                    OutputStream outputStream = new FileOutputStream(newFile);
                    IOUtils.copy(inputStream, outputStream);
                    inputStream.close();
                    outputStream.close();
                    return newFile.getAbsolutePath();
                });
    }
    /**
     * 获取拼接后的路径
     * @param paths 路径数组，用于拼接
     * @return 拼接后的路径
     */
    public static String getPath(String ... paths) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0; i<paths.length; i++) {
            String tmpPath = paths[i];
            if (tmpPath.endsWith(File.separator)) {
                tmpPath = tmpPath.substring(0, tmpPath.length()-1);
            }
            if (i > 0 && tmpPath.startsWith(File.separator)) {
                tmpPath = tmpPath.substring(1);
            }
            stringBuilder.append(tmpPath).append(File.separator);
        }
        return stringBuilder.toString();
    }
}
