package cn.devtool.spring.util;


import cn.devtool.excel.ExcelReader;
import cn.devtool.excel.ExcelWriter;
import cn.devtool.excel.model.ExcelData;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;

/**
 * Excel 工具类
 *
 * @author chentiefeng
 * @created 2021/4/21 17:02
 */
public class ExcelUtils {
    private ExcelUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 读取Excel文件的数据
     * @param file  MultipartFile
     * @param sheetIndex sheet索引号
     * @return ExcelData
     * @throws IOException IOException
     */
    public static ExcelData read(MultipartFile file, int sheetIndex) throws IOException {
        ExcelData excelData = new ExcelData(file.getOriginalFilename());
        Workbook workbook = ExcelReader.getWorkbook(excelData.getFileName(), file.getInputStream());
        excelData.setSheets(Collections.singletonList(ExcelReader.getSheet(workbook, sheetIndex)));
        return excelData;
    }
    /**
     * 读取Excel文件的数据
     * @param file  MultipartFile
     * @return ExcelData
     * @throws IOException IOException
     */
    public static ExcelData read(MultipartFile file) throws IOException {
        return read(file, 0);
    }


    /**
     * 读取Excel文件的数据
     * @param file File
     * @return ExcelData
     * @throws IOException IOException
     */
    public static ExcelData read(File file) throws IOException {
        return ExcelReader.read(file);
    }

    /**
     * 写入Excel文件
     * @param file File excel文件
     * @param excelData excel数据
     * @throws IOException IOException
     */
    public static void write(File file, ExcelData excelData) throws IOException {
        ExcelWriter.write(file, excelData);
    }

    /**
     * 写入输出流
     * @param outputStream 输出流
     * @param excelData excel数据
     * @throws IOException IOException
     */
    public static void write(OutputStream outputStream, ExcelData excelData) throws IOException {
        ExcelWriter.write(outputStream, excelData);
    }
}
