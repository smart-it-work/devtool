package cn.devtool.spring.exception;

/**
 * 参数不合法异常
 *
 * @author chentiefeng
 * @created 2021/2/25 上午12:51
 */
public class ArgsNotValidException extends RuntimeException {
    public ArgsNotValidException(String message) {
        super(message);
    }
}
