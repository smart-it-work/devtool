package cn.devtool.spring.exception;

/**
 * 没有找到异常
 *
 * @author chentiefeng
 * @created 2021/7/14 18:18
 */
public class NotFoundException  extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }
}
