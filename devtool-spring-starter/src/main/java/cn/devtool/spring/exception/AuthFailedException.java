package cn.devtool.spring.exception;

/**
 * 认证失败异常
 *
 * @author chentiefeng
 * @created 2021/2/25 上午12:51
 */
public class AuthFailedException extends RuntimeException {
    public AuthFailedException(String message) {
        super(message);
    }
}
