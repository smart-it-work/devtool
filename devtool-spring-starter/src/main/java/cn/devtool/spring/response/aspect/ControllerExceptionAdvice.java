package cn.devtool.spring.response.aspect;

import cn.devtool.spring.exception.AuthFailedException;
import cn.devtool.spring.request.context.RequestInfoContextHolder;
import cn.devtool.spring.response.model.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartException;

import java.net.BindException;

/**
 * 拦截所有的Controller异常
 *
 * @author chentiefeng
 * @created 2020-11-27 15:00
 */
@Slf4j
@ControllerAdvice
public class ControllerExceptionAdvice {
    private static final String ERROR_MSG = "Controller捕获到Exception异常[requestId:{}, user:{}]";

    /**
     * 拦截自定义异常
     * @param e 异常
     * @return ResponseResult
     */
    @ResponseBody
    @ExceptionHandler(value = AuthFailedException.class)
    public ResponseResult<Object> authFailedHandler(AuthFailedException e) {
        log.error(ERROR_MSG, RequestInfoContextHolder.getContext().getRequestId(), RequestInfoContextHolder.getContext().getUsername(), e);
        return ResponseResult.fail(401, e.getMessage());
    }
    @ResponseBody
    @ExceptionHandler(value = MultipartException.class)
    public ResponseResult<Object>  multipartHandler(MultipartException e) {
        log.error(ERROR_MSG, RequestInfoContextHolder.getContext().getRequestId(), RequestInfoContextHolder.getContext().getUsername(), e);
        return ResponseResult.fail("上传文件不能为空！", e);
    }
    /**
     * 参数验证失败异常
     * @param e 异常
     * @return ResponseResult
     */
    @ResponseBody
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseResult<Object> methodArgumentNotValidException(MethodArgumentNotValidException e) {
        log.error(ERROR_MSG, RequestInfoContextHolder.getContext().getRequestId(), RequestInfoContextHolder.getContext().getUsername(), e);
        BindingResult bindingResult = e.getBindingResult();
        if (bindingResult.hasErrors()) {
            StringBuilder errorMsg = new StringBuilder();
            for (ObjectError error : bindingResult.getAllErrors()) {
                errorMsg.append(error.getDefaultMessage()).append("<br>");
            }
            return ResponseResult.fail(errorMsg.toString(), e);
        }
        return ResponseResult.fail(e.getMessage(), e);
    }
    /**
     * 参数验证失败异常
     * @param e  异常
     * @return ResponseResult
     */
    @ExceptionHandler(BindException.class)
    public ResponseResult<Object> handleMethodArgumentNotValidException(BindException e) {
        log.error(ERROR_MSG, RequestInfoContextHolder.getContext().getRequestId(), RequestInfoContextHolder.getContext().getUsername(), e);
        return ResponseResult.fail(e.getMessage(), e);
    }

    /**
     * 全局异常捕捉处理
     * @param e 异常
     * @return ResponseResult
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ResponseResult<Object> javaExceptionHandler(Exception e) {
        log.error(ERROR_MSG, RequestInfoContextHolder.getContext().getRequestId(), RequestInfoContextHolder.getContext().getUsername(), e);
        return ResponseResult.fail(e.getMessage(), e);
    }
}
