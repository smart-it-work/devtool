package cn.devtool.spring.response.model;


import cn.devtool.spring.enums.ResultCodeType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 接口返回对象
 *
 * @author chentiefeng
 * @created 2020-10-21 15:03
 */
public class ResponseResult<T> {
    /**
     * 成功标志
     */
    private boolean success;
    /**
     * 响应码
     */
    private int code;
    /**
     * 提示信息
     */
    private String message;
    /**
     * 详细信息
     */
    @JsonInclude(Include.NON_NULL)
    private String detail;
    /**
     * 返回数据
     */
    @JsonInclude(Include.NON_NULL)
    private T data;
    public ResponseResult(){}
    private ResponseResult(boolean success, int code, String message, T data) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 返回成功对象
     * @param <T> 返回数据的数据类型
     * @return ResponseResult对象
     */
    public static <T> ResponseResult<T> ok() {
        return ok(null);
    }

    /**
     * 返回成功对象
     * @param data 返回数据
     * @param <T> 返回数据的数据类型
     * @return ResponseResult对象
     */
    public static <T> ResponseResult<T> ok(T data) {
        return ok(ResultCodeType.OK.getCode(), ResultCodeType.OK.getMessage(), data);
    }

    /**
     * 返回成功对象
     * @param code 返回码
     * @param message 返回信息
     * @param data 返回数据
     * @param <T> 返回数据的数据类型
     * @return ResponseResult对象
     */
    public static <T> ResponseResult<T> ok(int code, String message, T data) {
        return newInstance(true, code, message, data);
    }

    /**
     * 返回成失败对象
     * @param codeType 失败码类型
     * @param <T> 返回数据的数据类型
     * @return ResponseResult对象
     */
    public static <T> ResponseResult<T> fail(ResultCodeType codeType) {
        return fail(codeType.getCode(), codeType.getMessage());
    }

    /**
     * 返回成失败对象
     * @param message 返回信息
     * @param <T> 返回数据的数据类型
     * @return ResponseResult对象
     */
    public static <T> ResponseResult<T> fail(String message) {
        return fail(500, message);
    }

    /**
     * 返回成失败对象
     * @param code 返回码
     * @param message 返回信息
     * @param <T> 返回数据的数据类型
     * @return ResponseResult对象
     */
    public static <T> ResponseResult<T> fail(int code, String message) {
        return fail(code, message, null);
    }


    /**
     * 返回失败对象
     * @param code 返回码
     * @param message 返回信息
     * @param data 返回数据
     * @param <T> 返回数据的数据类型
     * @return ResponseResult对象
     */
    public static <T> ResponseResult<T> fail(int code, String message, T data) {
        return newInstance(false, code, message, data);
    }

    /**
     * 返回失败对象
     * @param message 信息
     * @param e 异常
     * @return  ResponseResult
     */
    public static ResponseResult<Object> fail(String message, Throwable e) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter, true);
        e.printStackTrace(printWriter);
        printWriter.flush();
        stringWriter.flush();
        String errorDetail = stringWriter.toString();
        ResponseResult<Object> responseResult  = newInstance(false, 500, message, null);
        responseResult.setDetail(errorDetail);
        return responseResult;
    }
    /**
     * 实例化一个ResponseResult对象.
     * @param success 成功标志
     * @param code 返回码
     * @param message 返回信息
     * @param data 返回数据
     * @param <T> 返回数据的数据类型
     * @return ResponseResult对象
     */
    public static <T> ResponseResult<T> newInstance(boolean success, int code, String message, T data) {
        return new ResponseResult<>(success, code, message, data);
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
