package cn.devtool.spring.response.aspect;

import cn.devtool.spring.response.annotation.EnableResponseResult;
import cn.devtool.spring.response.model.ResponseResult;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.lang.reflect.Method;

/**
 * Controller结果拦截
 *
 * @author chentiefeng
 * @created 2020-12-29 10:21
 */
@ControllerAdvice
public class ControllerResultAdvice implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        Method method = returnType.getMethod();
        Class<?> clazz = method.getDeclaringClass();
        if (converterType.equals(StringHttpMessageConverter.class)) {
            return false;
        }
        //判断是否在类对象上添加了注解
        return clazz.isAnnotationPresent(EnableResponseResult.class) || method.isAnnotationPresent(EnableResponseResult.class);
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        return body instanceof ResponseResult ? body : ResponseResult.ok(body);
    }
}
