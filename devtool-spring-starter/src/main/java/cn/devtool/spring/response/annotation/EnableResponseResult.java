package cn.devtool.spring.response.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用来Controller的返回结果是否需要对结果进行封装，封装为 {@link cn.devtool.spring.response.model.ResponseResult }
 * 用来标注Controller或者Controller的Method
 * @author chentiefeng
 * @created 2020-12-29 10:15
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableResponseResult {
}
