package cn.devtool.spring.request.model;


/**
 * 认证信息 Context
 * @author chentiefeng
 * @created 2021/7/7 10:05
 */
public interface AuthInfoContext {
    /**
     * 用户id
     * @return 用户的id信息
     */
    String getUserId();

    /**
     * 用户名
     * @return 用户名
     */
    String getUsername();
    /**
     * token
     * @return token
     */
    String getToken();
}
