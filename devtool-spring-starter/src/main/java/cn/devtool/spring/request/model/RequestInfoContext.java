package cn.devtool.spring.request.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

/**
 * Request信息 Context
 *
 * @author chentiefeng
 * @created 2021/7/8 14:25
 */
@Setter
public class RequestInfoContext implements AuthInfoContext, TrackInfoContext {
    /**
     * 时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime requestTime;
    /**
     * 本次的请求id
     */
    private String requestId;
    /**
     * 是否track
     */
    private boolean track = false;

    /**
     * 用户id
     */
    private String userId;
    /**
     * 用户
     */
    private String username;
    /**
     * token
     */
    private String token;

    /**
     * 附加参数，用于使用者可以添加自定义的参数
     */
    private Map<String, Object> additionalData;

    /**
     * 获取附加参数，附加参数主要是使用者添加的自定义参数
     * @return 附加参数
     */
    public Map<String, Object> getAdditionalData() {
        if (Objects.isNull(this.additionalData)) {
            return Collections.emptyMap();
        }
        return this.additionalData;
    }

    @Override
    public String getUserId() {
        return this.userId;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public String getToken() {
        return this.token;
    }

    @Override
    public LocalDateTime getRequestTime() {
        return this.requestTime;
    }

    @Override
    public String getRequestId() {
        return this.requestId;
    }

    @Override
    public boolean isTrack() {
        return this.track;
    }
}
