package cn.devtool.spring.request.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 追踪信息 Context
 *
 * @author chentiefeng
 * @created 2021/7/8 14:24
 */
public interface TrackInfoContext {
    /**
     * 请求时间
     * @return 请求的当前时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime getRequestTime();

    /**
     * 请求id
     * @return 本次的请求id
     */
    String getRequestId();

    /**
     * 是否 track
     * @return 是否 track
     */
    boolean isTrack();
}
