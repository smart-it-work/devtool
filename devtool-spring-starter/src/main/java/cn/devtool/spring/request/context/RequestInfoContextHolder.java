package cn.devtool.spring.request.context;

import cn.devtool.spring.request.model.RequestInfoContext;
import org.springframework.core.NamedInheritableThreadLocal;
import org.springframework.core.NamedThreadLocal;

/**
 * Request信息 Context
 *
 * @author chentiefeng
 * @created 2021/7/7 10:16
 */
public class RequestInfoContextHolder {
    private RequestInfoContextHolder() {
        throw new IllegalStateException("Utility class");
    }
    private static final ThreadLocal<RequestInfoContext> REQUEST_CONTEXT_HOLDER =
            new NamedThreadLocal<>("Request Info context");

    private static final ThreadLocal<RequestInfoContext> INHERITABLE_REQUEST_CONTEXT_HOLDER =
            new NamedInheritableThreadLocal<>("Request Info Inheritable context");

    /**
     * 获取 RequestInfoContext
     * @return RequestInfoContext
     */
    public static RequestInfoContext getContext() {
        RequestInfoContext context = REQUEST_CONTEXT_HOLDER.get();
        if (context == null) {
            context = INHERITABLE_REQUEST_CONTEXT_HOLDER.get();
        }
        return context;
    }

    /**
     * 设置 RequestInfoContext
     * @param context RequestInfoContext
     */
    public static void setContext(RequestInfoContext context) {
        setContext(context, false);
    }

    /**
     * 设置 RequestInfoContext
     * @param context RequestInfoContext
     * @param inheritable 是否 Inheritable
     */
    public static void setContext(RequestInfoContext context, boolean inheritable) {
        if (context == null) {
            resetContext();
        } else {
            if (inheritable) {
                INHERITABLE_REQUEST_CONTEXT_HOLDER.set(context);
                REQUEST_CONTEXT_HOLDER.remove();
            } else {
                REQUEST_CONTEXT_HOLDER.set(context);
                INHERITABLE_REQUEST_CONTEXT_HOLDER.remove();
            }
        }
    }

    /**
     * 重置当前线程的 RequestInfoContext
     */
    public static void resetContext() {
        REQUEST_CONTEXT_HOLDER.remove();
        INHERITABLE_REQUEST_CONTEXT_HOLDER.remove();
    }
}
