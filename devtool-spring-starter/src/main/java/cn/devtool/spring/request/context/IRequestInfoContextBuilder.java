package cn.devtool.spring.request.context;

import cn.devtool.spring.request.model.RequestInfoContext;

import javax.servlet.http.HttpServletRequest;


/**
 * RequestInfoContext构造器
 *
 * @author chentiefeng
 * @created 2021/7/7 10:47
 */
public interface IRequestInfoContextBuilder {
    /**
     * 构建 RequestInfoContext
     * @param request http请求
     * @return RequestInfoContext
     */
    RequestInfoContext build(HttpServletRequest request);
}
