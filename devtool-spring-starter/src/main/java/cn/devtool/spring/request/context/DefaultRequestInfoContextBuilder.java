package cn.devtool.spring.request.context;

import cn.devtool.spring.request.model.RequestInfoContext;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 *  默认的RequestInfoContextBuilder
 * @author chentiefeng
 * @created 2021/7/7 10:50
 */
public class DefaultRequestInfoContextBuilder implements IRequestInfoContextBuilder {
    /**
     * 请求id
     */
    public static final String HEADER_REQUEST_ID = "request-id";
    /**
     * track
     */
    public static final String HEADER_TRACK_KEY = "TRACK";
    /**
     * track的默认值
     */
    public static final String HEADER_TRACK_DEFAULT_VALUE = "true";
    /**
     * 默认的token字段
     */
    public static final String DEFAULT_HEADER_TOKEN_KEY = "token";
    /**
     * token
     */
    protected String tokenKey;

    /**
     * 构造方法
     * @param tokenKey token在Header中的字段名
     */
    public DefaultRequestInfoContextBuilder(String tokenKey) {
        this.tokenKey = tokenKey;
    }


    @Override
    public RequestInfoContext build(HttpServletRequest request) {
        RequestInfoContext requestInfoContext = new RequestInfoContext();
        String debugFlag = request.getHeader(HEADER_TRACK_KEY);
        if (HEADER_TRACK_DEFAULT_VALUE.equalsIgnoreCase(debugFlag)) {
            requestInfoContext.setTrack(true);
        }
        //取出request请求中的id, 如果没有则新生成
        String requestId = request.getHeader(HEADER_REQUEST_ID);
        if (requestId ==null || requestId.trim().length() ==0) {
            requestId = UUID.randomUUID().toString().replace("-","");
        }
        requestInfoContext.setRequestId(requestId);
        requestInfoContext.setRequestTime(LocalDateTime.now());
        requestInfoContext.setToken(request.getHeader(tokenKey));
        return requestInfoContext;
    }
}
