package cn.devtool.spring.request.context;

import cn.devtool.spring.request.model.RequestInfoContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * Request信息 监听
 *
 * @author chentiefeng
 * @created 2021/7/7 10:39
 */
public class RequestInfoContextListener implements ServletRequestListener {

    /**
     * 自动注入 IRequestInfoContextBuilder
     */
    @Autowired(required = false)
    private IRequestInfoContextBuilder requestInfoContextBuilder;

    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
        RequestInfoContext requestInfoContext = RequestInfoContextHolder.getContext();
        if (requestInfoContext != null) {
            RequestInfoContextHolder.resetContext();
        }
    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        HttpServletRequest request = (HttpServletRequest) servletRequestEvent.getServletRequest();
        if (Objects.isNull(requestInfoContextBuilder)) {
            requestInfoContextBuilder = new DefaultRequestInfoContextBuilder(DefaultRequestInfoContextBuilder.DEFAULT_HEADER_TOKEN_KEY);
        }
        RequestInfoContextHolder.setContext(requestInfoContextBuilder.build(request));
    }
}
