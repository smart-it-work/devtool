package cn.devtool.spring.log.clientlog;

import cn.devtool.spring.log.clientlog.model.FeignClientLog;

/**
 * Feign客户端日志 操作
 *
 * @author chentiefeng
 * @created 2021/7/16 14:45
 */
public interface IFeignClientLogMapper {
    /**
     * 保存FeignClient请求日志
     * @param clientLog FeignClient请求日志
     */
    void save(FeignClientLog clientLog);
}
