package cn.devtool.spring.log.urllog;

import cn.devtool.core.util.CheckUtils;
import cn.devtool.core.util.JsonUtils;
import cn.devtool.spring.log.urllog.model.RequestLog;
import cn.devtool.spring.request.context.RequestInfoContextHolder;
import cn.devtool.spring.request.model.RequestInfoContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.AbstractRequestLoggingFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * url日志记录
 *
 * @author chentiefeng
 * @created 2021/7/16 11:33
 */
@WebFilter(filterName = "UrlLogFilter", urlPatterns = "/*")
@Order(1)
public class RequestLogFilter extends AbstractRequestLoggingFilter {

    @Autowired
    private IRequestLogMapper requestUrlLogMapper;
    /**
     * 日志不记录的url集合
     */
    protected static final Set<String> IGNORE_URLS = Stream.of("/favicon.ico").collect(Collectors.toSet());
    @Override
    protected void beforeRequest(HttpServletRequest httpServletRequest, String requestInfo) {
        doLog(httpServletRequest, false);
    }

    @Override
    protected void afterRequest(HttpServletRequest httpServletRequest, String requestInfo) {
        doLog(httpServletRequest,true);
    }

    protected void doLog(HttpServletRequest request, boolean afterRequestFlag) {
        String requestUrl = request.getRequestURI();
        if (!afterRequestFlag && IGNORE_URLS.stream().noneMatch(requestUrl::contains)) {
            RequestInfoContext requestInfoContext = RequestInfoContextHolder.getContext();
            RequestLog requestUrlLog = new RequestLog();
            requestUrlLog.setId(requestInfoContext.getRequestId());
            requestUrlLog.setUrl(requestUrl);
            requestUrlLog.setUser(requestInfoContext.getUsername());
            requestUrlLog.setRequestTime(requestInfoContext.getRequestTime());
            ContentCachingRequestWrapper wrapper =new ContentCachingRequestWrapper(request);
            Enumeration<String> paramNames = wrapper.getParameterNames();
            Map<String, Object> paramMap = new HashMap<>(wrapper.getParameterMap().size());
            while (paramNames.hasMoreElements()) {
                String paramName = paramNames.nextElement();
                String[] paramValue = wrapper.getParameterValues(paramName);
                if (paramValue.length == 1) {
                    paramMap.put(paramName, paramValue[0]);
                } else {
                    paramMap.put(paramName, paramValue);
                }
            }
            requestUrlLog.setRequestParams(CheckUtils.checkValueException(() -> JsonUtils.toJson(paramMap), null));
            requestUrlLog.setRequestBody(CheckUtils.checkValueException(() -> wrapper.getReader().readLine(), null));
            requestUrlLogMapper.save(requestUrlLog);
        }
    }


}
