package cn.devtool.spring.log.urllog.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * Request请求日志记录
 *
 * @author chentiefeng
 * @created 2021/7/16 13:42
 */
@Data
public class RequestLog {
    /**
     * 唯一主键
     */
    private String id;
    /**
     * url
     */
    private String url;
    /**
     * 用户
     */
    private String user;
    /**
     * 请求时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime requestTime;
    /**
     *  请求参数
     */
    private String requestParams;
    /**
     * 请求内容
     */
    private String requestBody;
}
