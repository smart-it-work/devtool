package cn.devtool.spring.log.clientlog.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * Feign客户端日志
 *
 * @author chentiefeng
 * @created 2021/7/16 14:46
 */
@Data
public class FeignClientLog {
    /**
     * 唯一主键
     */
    private String id;
    /**
     * 批次号id
     */
    private String groupId;
    /**
     * 用户
     */
    private String user;
    /**
     * 客户端服务名称
     */
    private String clientServiceName;
    /**
     * 客户端服务URL
     */
    private String clientServiceUrl;
    /**
     * 客户端服务Path
     */
    private String clientServicePath;
    /**
     * 客户端类
     */
    private String clientClass;
    /**
     * 客户端Url
     */
    private String requestUrl;
    /**
     *  请求类型
     */
    private String requestMethod;
    /**
     *  请求参数
     */
    private String requestParams;
    /**
     * 请求开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;
    /**
     * 请求结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;
    /**
     * 请求是否成功
     */
    private boolean success = true;
    /**
     * 请求失败的下提示信息
     */
    private String message;
    /**
     * 请求失败的详细提示信息
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String detail;
}
