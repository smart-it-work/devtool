package cn.devtool.spring.log.clientlog;

import cn.devtool.core.util.CheckUtils;
import cn.devtool.core.util.JsonUtils;
import cn.devtool.core.util.StringUtils;
import cn.devtool.spring.log.clientlog.model.FeignClientLog;
import cn.devtool.spring.request.context.RequestInfoContextHolder;
import cn.devtool.spring.request.model.RequestInfoContext;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

/**
 * Feign客户端日志切片
 *
 * @author chentiefeng
 * @created 2021/7/16 14:44
 */
@Slf4j
@Aspect
@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true)
public class FeignClientLogAspect {

    @Autowired(required = false)
    private IFeignClientLogMapper feignClientLogMapper;


    @Around("@within(org.springframework.cloud.openfeign.FeignClient)")
    public Object handleException(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        if (Objects.nonNull(feignClientLogMapper)) {
            FeignClientLog clientLog = new FeignClientLog();
            clientLog.setStartTime(LocalDateTime.now());
            clientLog.setId(UUID.randomUUID().toString());
            RequestInfoContext requestInfoContext = RequestInfoContextHolder.getContext();
            clientLog.setGroupId(requestInfoContext.getRequestId());
            clientLog.setUser(requestInfoContext.getUsername());
            Class<?> feignClientClass = getFeignInterfaceClass(proceedingJoinPoint);
            if (Objects.nonNull(feignClientClass)) {
                RequestMappingInfo requestMappingInfo = CheckUtils.checkValueException(() -> geRequestInfo(proceedingJoinPoint, feignClientClass), null);
                clientLog.setRequestUrl(requestMappingInfo.getUrl());
                clientLog.setRequestMethod(Objects.nonNull(requestMappingInfo.getMethod())? requestMappingInfo.getMethod().name(): null);
                clientLog.setClientServiceName(CheckUtils.checkValueException(() -> getFeignAnnotationValue(feignClientClass, "name"), null));
                clientLog.setClientServicePath(CheckUtils.checkValueException(() -> getFeignAnnotationValue(feignClientClass, "path"), null));
                clientLog.setClientServiceUrl(CheckUtils.checkValueException(() -> getFeignAnnotationValue(feignClientClass, "url"), null));
            }
            clientLog.setClientClass(proceedingJoinPoint.getSignature().toLongString());
            clientLog.setRequestParams(CheckUtils.checkValueException(() -> JsonUtils.toJson(Arrays.asList(proceedingJoinPoint.getArgs())), null));
            try {
                return proceedingJoinPoint.proceed();
            } catch (Throwable e) {
                clientLog.setSuccess(false);
                clientLog.setMessage(e.getMessage());
                clientLog.setDetail(StringUtils.getExceptionToString(e));
                throw e;
            } finally {
                clientLog.setEndTime(LocalDateTime.now());
                feignClientLogMapper.save(clientLog);
            }
        } else {
            try {
                return proceedingJoinPoint.proceed();
            } catch (Throwable e) {
                log.error("Client捕获到Exception异常[方法：{}， 参数：{}]", proceedingJoinPoint.getSignature(), proceedingJoinPoint.getArgs(), e);
                throw e;
            }
        }
    }

    /**
     * 获取FeignClient注解的值
     * @param feignClientClass feignClient 接口类
     * @param fieldName FeignClient注解属性字段名称
     * @return FeignClient注解属性字段值
     */
    private String getFeignAnnotationValue(Class<?> feignClientClass, String fieldName) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<Annotation> feignClientAnnotationClass = (Class<Annotation>) Class.forName("org.springframework.cloud.openfeign.FeignClient");
        Annotation annotation = feignClientClass.getAnnotation(feignClientAnnotationClass);
        Method method = feignClientAnnotationClass.getMethod(fieldName);
        Object value = method.invoke(annotation);
        return Objects.nonNull(value)? String.valueOf(value): null;
    }
    /**
     * 获取FeignClient的接口类
     * @param joinPoint  切片切入点
     * @return FeignClient接口
     * @throws InvocationTargetException InvocationTargetException
     * @throws IllegalAccessException IllegalAccessException
     * @throws NoSuchFieldException NoSuchFieldException
     * @throws NoSuchMethodException NoSuchMethodException
     */
    private Class<?> getFeignInterfaceClass(ProceedingJoinPoint joinPoint) {
        try {
            // 获取目标对象
            Object targetObj = joinPoint.getTarget();
            InvocationHandler invocationHandler = Proxy.getInvocationHandler(targetObj);
            Field targetField = invocationHandler.getClass().getDeclaredField("target");
            targetField.setAccessible(true);
            Object objectTarget = targetField.get(invocationHandler);
            Method typeMethod = objectTarget.getClass().getMethod("type");
            // 获取到接口类
            return (Class<?>) typeMethod.invoke(objectTarget);
        } catch (Exception e) {
            log.error("获取FeignClient的接口类出现异常，[方法：{}， 参数：{}]", joinPoint.getSignature(), joinPoint.getArgs(), e);
            return null;
        }
    }

    /**
     * 获取FeignClient类中方法上的注解url
     * @param joinPoint 切片切入点
     * @return  方法上的注解url
     * @throws NoSuchMethodException NoSuchMethodException
     */
    private RequestMappingInfo geRequestInfo(ProceedingJoinPoint joinPoint, Class<?> feignClientClass) throws NoSuchMethodException {
        // 获取方法名
        String methodName = joinPoint.getSignature().getName();
        // 拿到方法对应的参数类型
        Class<?>[] parameterTypes = ((MethodSignature) joinPoint.getSignature()).getParameterTypes();
        // 根据类、方法、参数类型（重载）获取到方法的具体信息
        Method objMethod = feignClientClass.getMethod(methodName, parameterTypes);
        GetMapping getMapping = objMethod.getDeclaredAnnotation(GetMapping.class);
        if (Objects.nonNull(getMapping)) {
            return new RequestMappingInfo(getMapping.value()[0], RequestMethod.GET);
        }
        PostMapping postMapping = objMethod.getDeclaredAnnotation(PostMapping.class);
        if (Objects.nonNull(postMapping)) {
            return new RequestMappingInfo(postMapping.value()[0], RequestMethod.POST);
        }
        PutMapping putMapping = objMethod.getDeclaredAnnotation(PutMapping.class);
        if (Objects.nonNull(putMapping)) {
            return new RequestMappingInfo(putMapping.value()[0], RequestMethod.PUT);
        }
        DeleteMapping deleteMapping = objMethod.getDeclaredAnnotation(DeleteMapping.class);
        if (Objects.nonNull(deleteMapping)) {
            return new RequestMappingInfo(deleteMapping.value()[0], RequestMethod.DELETE);
        }
        RequestMapping requestMapping = objMethod.getDeclaredAnnotation(RequestMapping.class);
        if (Objects.nonNull(requestMapping)) {
            return new RequestMappingInfo(requestMapping.value()[0], null);
        }
        return new RequestMappingInfo("", null);
    }

    @Data
    @AllArgsConstructor
    public static class RequestMappingInfo {
        private String url;
        private RequestMethod method;
    }
}
