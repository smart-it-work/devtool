package cn.devtool.spring.log.datalog;

import cn.devtool.spring.log.datalog.model.DataChangeLog;

import java.util.List;

/**
 * 数据变化日志记录 操作接口
 *
 * @author chentiefeng
 * @created 2021/7/15 17:17
 */
public interface IDataChangeLogMapper {
    /**
     * 保存日志
     * @param log 日志
     */
    void save(DataChangeLog log);

    /**
     * 通过数据id查询
     * @param dataId 批次id
     * @return 数据变化日志（集合）
     */
    List<DataChangeLog> selectByDataId(String dataId);

    /**
     * 通过批次id查询
     * @param groupId 批次id
     * @return 数据变化日志（集合）
     */
    List<DataChangeLog> selectByGroup(String groupId);

    /**
     * 通过表名查询
     * @param tableName 表名
     * @return 数据变化日志（集合）
     */
    List<DataChangeLog> selectByTable(String tableName);

    /**
     * 通过表名查询最近一次的变化日志
     * @param tableName 表名
     * @return 最近一次的变化日志
     */
    DataChangeLog selectLastOneByTable(String tableName);
}
