package cn.devtool.spring.log.datalog.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 数据改变日志
 *
 * @author chentiefeng
 * @created 2021/7/15 15:11
 */
@Data
public class DataChangeLog {
    /**
     * 唯一主键
     */
    private String id;
    /**
     * 批次id，可能出现一次修改多个表，多条数据
     */
    private String groupId;
    /**
     * 要修改的表名
     */
    private String tableName;
    /**
     * 修改的用户
     */
    private String modifyUser;
    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modifyTime;
    /**
     * 数据的唯一id，用于查询单一数据时使用
     */
    private String dataId;
    /**
     * 旧值
     */
    private String oldValue;
    /**
     * 新值
     */
    private String newValue;
    /**
     * 来源class
     */
    private String sourceClass;
    /**
     * 来源Method
     */
    private String sourceMethod;
    /**
     * 来源行号
     */
    private int sourceLineNumber;
    /**
     * 来源文件名
     */
    private String sourceFileName;
}
