package cn.devtool.spring.log.datalog;

import cn.devtool.core.util.CheckUtils;
import cn.devtool.core.util.JsonUtils;
import cn.devtool.core.util.StackTraceUtils;
import cn.devtool.spring.exception.NotFoundException;
import cn.devtool.spring.log.datalog.model.DataChangeLog;
import cn.devtool.spring.request.context.RequestInfoContextHolder;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * 数据日志服务
 *
 * @author chentiefeng
 * @created 2021/7/15 15:20
 */
public class DataChangeLogService {

    @Autowired
    private IDataChangeLogMapper dataChangeLogSaveMapper;

    /**
     * 保存数据变化日志
     * @param tableName 表名
     * @param dataId 数据的唯一id，用于标识那条数据
     * @param oldValue 旧值
     * @param newValue 新值
     * @throws NotFoundException 找不到 IDataChangeLogSaveMapper的实现类
     */
    public void saveChangeLog(String tableName, String dataId, Object oldValue, Object newValue) throws NotFoundException {
        DataChangeLog dataChangeLog = new DataChangeLog();
        dataChangeLog.setId(UUID.randomUUID().toString());
        dataChangeLog.setGroupId(RequestInfoContextHolder.getContext().getRequestId());
        dataChangeLog.setTableName(tableName);
        dataChangeLog.setModifyUser(RequestInfoContextHolder.getContext().getUsername());
        dataChangeLog.setModifyTime(LocalDateTime.now());
        dataChangeLog.setDataId(dataId);
        dataChangeLog.setOldValue(CheckUtils.checkValueException(() -> JsonUtils.toJson(oldValue), null));
        dataChangeLog.setNewValue(CheckUtils.checkValueException(() -> JsonUtils.toJson(newValue), null));
        StackTraceElement sourceStackTraceElement = StackTraceUtils.getPreviousStackTrace();
        assert sourceStackTraceElement != null;
        dataChangeLog.setSourceClass(sourceStackTraceElement.getClassName());
        dataChangeLog.setSourceMethod(sourceStackTraceElement.getMethodName());
        dataChangeLog.setSourceLineNumber(sourceStackTraceElement.getLineNumber());
        dataChangeLog.setSourceFileName(sourceStackTraceElement.getFileName());
        if (dataChangeLogSaveMapper == null) {
            throw new NotFoundException("没有找到IDataChangeLogSaveMapper的实现类.");
        }
        dataChangeLogSaveMapper.save(dataChangeLog);
    }

    /**
     * 通过数据id查询
     * @param dataId 批次id
     * @return 数据变化日志（集合）
     */
    public List<DataChangeLog> selectByDataId(String dataId) {
        return dataChangeLogSaveMapper.selectByDataId(dataId);
    }

    /**
     * 通过批次id查询
     * @param groupId 批次id
     * @return 数据变化日志（集合）
     */
    public List<DataChangeLog> selectByGroup(String groupId) {
        return dataChangeLogSaveMapper.selectByGroup(groupId);
    }

    /**
     * 通过表名查询
     * @param tableName 表名
     * @return 数据变化日志（集合）
     */
    public List<DataChangeLog> selectByTable(String tableName) {
        return dataChangeLogSaveMapper.selectByTable(tableName);
    }

    /**
     * 通过表名查询最近一次的变化日志
     * @param tableName 表名
     * @return 最近一次的变化日志
     */
    public DataChangeLog selectLastOneByTable(String tableName) {
        return dataChangeLogSaveMapper.selectLastOneByTable(tableName);
    }
}
