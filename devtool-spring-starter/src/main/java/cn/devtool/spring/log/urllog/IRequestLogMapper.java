package cn.devtool.spring.log.urllog;

import cn.devtool.spring.log.urllog.model.RequestLog;

/**
 * RequestLog操作接口
 *
 * @author chentiefeng
 * @created 2021/7/16 13:54
 */
public interface IRequestLogMapper {
    /**
     * 保存Request请求日志
     * @param requestUrlLog Request请求日志
     */
    void save(RequestLog requestUrlLog);
}
