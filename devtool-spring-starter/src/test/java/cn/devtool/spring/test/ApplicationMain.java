package cn.devtool.spring.test;

import cn.devtool.spring.log.urllog.IRequestLogMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @author chentiefeng
 * @created 2021/7/12 18:25
 */
@SpringBootApplication
public class ApplicationMain {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationMain.class, args);
    }

    @Bean
    public IRequestLogMapper requestLogMapper() {
        return item -> System.out.println(item);
    }
}
