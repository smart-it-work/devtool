package cn.devtool.spring.test;

import cn.devtool.spring.enums.HttpMediaType;
import cn.devtool.spring.log.datalog.DataChangeLogService;
import cn.devtool.spring.util.ResponseUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author chentiefeng
 * @created 2021/7/12 16:35
 */
@RestController
public class Test2Controller {
    @Autowired(required = false)
    private DataChangeLogService dataLogService;
    @GetMapping("/test/log")
    public void test() {
        dataLogService.saveChangeLog("test", "123", "test", "test");
    }
    @GetMapping("/test/info/{user}")
    public String test2(HttpServletRequest request, @PathVariable String user) {
       return "success" + user;
    }
    @PostMapping("/test/info")
    public String test3(HttpServletRequest request, @RequestParam String user) {
        return "success" + user;
    }
    @GetMapping("/test/download")
    public void testDownload(HttpServletResponse response) {
        try {
            InputStream in = new FileInputStream("C:\\Users\\imche\\Desktop\\没找到so的sql1.txt");
            IOUtils.copy(in, ResponseUtils.download(response, HttpMediaType.TEXT_HTML_UTF8, "没找到so的sql1.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


        @GetMapping("/test/redire")
    public void testRedire(HttpServletRequest request, HttpServletResponse response) {
        ResponseUtils.redirect(request, response, "/test/download");
    }


    @GetMapping("/test/write")
    public void testWrite(HttpServletRequest request, HttpServletResponse response) {
        ResponseUtils.writeHtml(response, "<h1>Helloworld</h1><img src=\"http://localhost:8080/test/image\">");
    }

    @GetMapping("/test/image")
    public void testImage(HttpServletRequest request, HttpServletResponse response) {
        //ResponseUtils.writeImage(request, response, "C:\\Users\\imche\\Desktop\\doc\\images\\旋转图.gif");
        ResponseUtils.writeImage(request, response, "https://profile.csdnimg.cn/5/4/5/2_chentiefeng521", true);
    }
    @PostMapping("/test/upload")
    public void testUpload(HttpServletRequest req, @RequestParam("file") MultipartFile file, String user) {
        System.out.println();
    }
}
