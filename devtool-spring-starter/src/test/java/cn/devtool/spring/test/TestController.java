package cn.devtool.spring.test;

import cn.devtool.spring.response.annotation.EnableResponseResult;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chentiefeng
 * @created 2021/7/12 16:35
 */
@RestController
@RequestMapping("test")
public class TestController {

    @EnableResponseResult
    @GetMapping("/user")
    public User test() {
        User user = new User();
        user.setUsername("chentiefeng");
        user.setAge(10);
        return user;
    }
}
@Data
class User {
    private String username;
    private int age;
}
