package cn.devtool.core.model;

import lombok.Data;

/**
 * 处理结果返回类
 *
 * @author chentiefeng
 * @created 2019-12-09 13:38
 */
@Data
public class HandleResult<T> {
    /**
     * 成功标志
     */
    private boolean successFlag = false;

    /**
     * 编码
     */
    private String code;
    /**
     * 返回处理信息
     */
    private String message;

    /**
     * 详细处理信息，主要用于异常信息的返回
     */
    private String detailMessage;

    /**
     * 处理返回对象
     */
    private T data;

    /**
     * 处理成功
     * @param message 信息
     * @param <T> 返回值泛型
     * @return HandleResult
     */
    public static <T> HandleResult<T> ok(String message) {
        return  getInstance(true,message,null,null);
    }
    public static <T> HandleResult<T> ok(T data) {
        return  getInstance(true,null,null,data);
    }
    public static <T> HandleResult<T> ok(String message,T data) {
        return  getInstance(true,message,null,data);
    }


    /**
     * 处理失败
     * @param message 信息
     * @param detailMessage 详细消息
     * @param t 数据
     * @param <T> 返回值泛型
     * @return HandleResult
     */
    public static <T> HandleResult<T> fail(String message,String detailMessage,T t) {
       return  getInstance(false,message,detailMessage,t);
    }
    public static <T> HandleResult<T> fail(String message) {
        return  getInstance(false,message,null,null);
    }
    public static <T> HandleResult<T> fail(String message,String detailMessage) {
        return  getInstance(false,message,detailMessage,null);
    }
    public static <T> HandleResult<T> fail(String code,String message,String detailMessage) {
        return  getInstance(false,code,message,detailMessage,null);
    }

    private static <T> HandleResult<T> getInstance(boolean successFlag, String message,String detailMessage,T data){
        HandleResult<T> result=new HandleResult<>();
        result.setSuccessFlag(successFlag);
        result.setMessage(message);
        result.setDetailMessage(detailMessage);
        result.setData(data);
        return result;
    }
    private static <T> HandleResult<T> getInstance(boolean successFlag,String code, String message,String detailMessage,T data){
        HandleResult<T> result=new HandleResult<>();
        result.setSuccessFlag(successFlag);
        result.setMessage(message);
        result.setDetailMessage(detailMessage);
        result.setData(data);
        result.setCode(code);
        return result;
    }
}
