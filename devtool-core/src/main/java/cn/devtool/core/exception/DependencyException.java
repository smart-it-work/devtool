package cn.devtool.core.exception;

/**
 * 依赖异常
 *
 * @author chentiefeng
 * @created 2021/7/20 16:43
 */
public class DependencyException extends RuntimeException {
    public DependencyException(String message) {
        super(message);
    }
    public DependencyException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
