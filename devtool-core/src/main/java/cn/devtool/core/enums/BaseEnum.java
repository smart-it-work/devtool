package cn.devtool.core.enums;

import java.lang.reflect.Type;

/**
 * 枚举的基本定义类
 *
 * @author chentiefeng
 * @created 2019-10-16 16:35
 */

public interface BaseEnum extends Type {
    /**
     * 获取编码
     * @return 编码
     */
    String getCode();

    /**
     * 获取标题
     * @return 标题
     */
    String getTitle();
}
