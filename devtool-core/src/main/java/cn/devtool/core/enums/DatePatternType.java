package cn.devtool.core.enums;

import lombok.Getter;

/**
 * 日期格式枚举
 *
 * @author chentiefeng
 * @created 2020-10-22 15:38
 */
@Getter
public enum DatePatternType {
    /**
     * yyyyMM
     */
    YYYYMM("yyyyMM"),
    /**
     * yyyyMMdd
     */
    YYYYMMDD("yyyyMMdd"),
    /**
     * yyyyMMddHHmmss
     */
    YYYYMMDDHHMMSS("yyyyMMddHHmmss"),
    /**
     * yyyyMMddHHmmssSSS
     */
    YYYYMMDDHHMMSSSSS("yyyyMMddHHmmssSSS"),
    /**
     * yyyy-MM-dd
     */
    YYYY_MM_DD("yyyy-MM-dd"),
    /**
     * yyyy-MM-dd HH:mm:ss
     */
    YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss");

    private final String value;
    DatePatternType(String value) {
        this.value = value;
    }
}
