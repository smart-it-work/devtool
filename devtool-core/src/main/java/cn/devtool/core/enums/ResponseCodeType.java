package cn.devtool.core.enums;

import lombok.Getter;

/**
 * 错误码类型
 *
 * @author chentiefeng
 * @created 2021/1/20 19:44
 */
@Getter
public enum ResponseCodeType {
    /**
     * Success
     */
    SUCCESS(200, "Success"),
    /**
     * TIMEOUT
     */
    TIMEOUT(504, "Gateway timeout, please try again later."),
    /**
     * Internal server error
     */
    SERVER_ERROR(500, "Internal server error, please contact administrator."),
    /**
     * Bad Gateway
     */
    BAD_GATEWAY(502, "Bad Gateway, please contact administrator"),
    /**
     * external system failed
     */
    SERVICE_UNAVAILABLE(503, "The external system call Posting interface failed.");

    private int code;
    private String message;

    ResponseCodeType(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
