package cn.devtool.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据字典枚举
 *
 * @author chentiefeng
 * @created 2019-12-12 17:21
 */
@Target({ElementType.TYPE,ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DictEnum {
    /**
     * 编码（英文，一般为字段）
     * @return 名称
     */
    String code();

    /**
     * 标题(一般为具体含义)
     * @return 标题
     */
    String title();
}
