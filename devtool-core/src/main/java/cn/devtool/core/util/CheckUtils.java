package cn.devtool.core.util;


/**
 * 检查工具类
 *
 * @author chentiefeng
 * @created 2021/7/15 11:03
 */
public class CheckUtils {
    private CheckUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 校验是否为true，如果不为ture，抛出 falseException
     * @param trueFlag true值
     * @param falseException falseException
     */
    public static void checkTrue(boolean trueFlag, RuntimeException falseException) {
        if (!trueFlag) {
            throw falseException;
        }
    }

    /**
     * 检查带有exception的值，如果出现Exception，返回 defaultValue
     * @param function 获取数值的带有Exception的函数
     * @param defaultValue 默认值
     * @param <R> 结果类型
     * @return 结果值
     */
    public static <R> R checkValueException(CheckFunction<R> function, R defaultValue) {
        try {
            return function.apply();
        } catch (Throwable e) {
            return defaultValue;
        }
    }

    /**
     * 一个接口，用于在lambda中定义使用带有异常的方法
     * @param <R> 返回值
     */
    public interface CheckFunction<R> {
        /**
         * 用于执行，得到返回值
         * @return 结果
         * @throws Throwable Throwable
         */
        R apply() throws Throwable;
    }
}
