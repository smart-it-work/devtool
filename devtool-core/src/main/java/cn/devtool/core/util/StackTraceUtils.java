package cn.devtool.core.util;


/**
 * StackTrace工具类
 *
 * @author chentiefeng
 * @created 2021/7/15 15:59
 */
public class StackTraceUtils {
    private StackTraceUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 获取调用的来源StackTraceElement
     * @return StackTraceElement
     */
    public static StackTraceElement getPreviousStackTrace() {
        StackTraceElement[] stackTraceElements = new Throwable().getStackTrace();
        for (int i=0; i < stackTraceElements.length; i++) {
            StackTraceElement tmpStackTraceElement = stackTraceElements[i];
            if (tmpStackTraceElement.getClassName().equals(StackTraceUtils.class.getName())) {
                return stackTraceElements[i+2];
            }
        }
        return null;
    }

    /**
     * 获取调用的来源thread的某个类的第一个StackTraceElement
     * @param thread 来源thread
     * @param clazz 上一个某个类的StackTrace
     * @return StackTraceElement
     */
    public static StackTraceElement getPreviousStackTrace(Thread thread, Class<?> clazz) {
        StackTraceElement[] stackTraceElements = Thread.getAllStackTraces().get(thread);
        for (StackTraceElement tmpStackTraceElement : stackTraceElements) {
            if (tmpStackTraceElement.getClassName().equals(clazz.getName())) {
                return tmpStackTraceElement;
            }
        }
        return null;
    }
}
