package cn.devtool.core.util;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class工具类
 *
 * @author chentiefeng
 * @created 2021/7/14 14:23
 */
public class ClassUtils {
    private ClassUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 根据给定的类，获取字段名(下划线)字符串
     * @param clazz 需要获取字段名的类
     * @param <T> Class泛型
     * @return 字段名(下划线)字符串
     */
    public static <T> String getFiledStr(Class<T> clazz) {
        List<String> fieldNameList = getFieldNames(clazz);
        return fieldNameList.stream().map(StringUtils::lowerCamelToUnderscore).collect(Collectors.joining(","));
    }

    /**
     * 根据给定的类，获取字段名(下划线)集合
     * @param clazz 需要获取字段名的类'
     * @param <T> Class泛型
     * @return 字段名(下划线)字符串
     */
    public static <T> List<String> getUnderscoreFields(Class<T> clazz) {
        List<String> fieldNameList = getFieldNames(clazz);
        return fieldNameList.stream().map(StringUtils::lowerCamelToUnderscore).collect(Collectors.toList());
    }
    /**
     * 获取类的字段名集合
     * @param clazz 需要获取字段名的类
     * @param <T> Class泛型
     * @return 字段名集合
     */
    public static <T> List<String> getFieldNames(Class<T> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        List<String> fieldNameList = Arrays.stream(fields).map(Field::getName).collect(Collectors.toList());
        if (clazz.getSuperclass() != null) {
            Class<?> superClass = clazz.getSuperclass();
            Field[] superFields = superClass.getDeclaredFields();
            fieldNameList.addAll(Arrays.stream(superFields).map(Field::getName).filter(name -> !fieldNameList.contains(name)).collect(Collectors.toList()));
        }
        return fieldNameList;
    }

    /**
     * 获取字段集合
     * @param obj 对象
     * @return 字段集合
     */
    public static Field[] getFields(Object obj) {
        Class<?> clazz = obj.getClass();
        return clazz.getDeclaredFields();
    }

    /**
     * 检查类是否存在
     * @param className 类的全限定类名，如：org.apache.poi.xssf.usermodel.XSSFWorkbookFactory
     * @return 是否存在某个类
     */
    public static boolean checkExist(String className) {
        try {
            Class.forName(className);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

}
