package cn.devtool.core.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.List;

/**
 * 文件工具类
 *
 * @author chentiefeng
 * @created 2021/7/8 15:18
 */
@Slf4j
public class FileUtils {
    private FileUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 追加到文件(文件不存在，会自动创建)
     * @param path 文件Path
     * @param content 追加内容
     * @return 文件名
     * @throws IOException IO异常
     */
    public static String appendToFile(Path path, String content) throws IOException {
        try {
            Files.write(path, content.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE,StandardOpenOption.APPEND);
            return path.getFileName().toString();
        } catch (IOException e) {
            log.error("追加数据到文件出现异常，文件：{}，追加内容：{}", path.getFileName(), content);
            throw e;
        }
    }

    /**
     * 创建临时文件
     * @param prefix 临时文件前缀
     * @param suffix 临时文件后缀 如：.csv
     * @return 临时文件
     * @throws IOException IOException
     */
    public static File createTempFile(String prefix, String suffix) throws IOException {
        return File.createTempFile(prefix, suffix);
    }

    /**
     * 读取文件所有行
     * @param filePath 文件路径
     * @return 行集合
     * @throws IOException IO异常
     */
    public static List<String> readAllLine(String filePath) throws IOException {
        return Files.readAllLines(Paths.get(filePath));
    }

    /**
     * 保存行数据到文件
     * @param lines 行数据集合
     * @param saveFilePath 保存文件路径
     * @throws IOException IO异常
     */
    public static void saveToFile(List<String> lines, String saveFilePath) throws IOException {
        Files.write(Paths.get(saveFilePath),lines);
    }

    /**
     * 保存内容到文件
     * @param content 内容
     * @param saveFilePath 保存文件路径
     * @throws IOException IO异常
     */
    public static void saveToFile(String content, String saveFilePath) throws IOException {
        Files.write(Paths.get(saveFilePath), Collections.singletonList(content));
    }

    public static void appendToFile(String content, String saveFilePath) throws IOException {
        Files.write(Paths.get(saveFilePath), Collections.singletonList(content));
    }

    /**
     * 读取文件为字符串，如JSON文件
     * @param filePath 文件路径
     * @param needLinebreak 是否需要换行符
     * @return 字符串
     * @throws IOException IOException
     */
    public static String readToString(String filePath, boolean needLinebreak) throws IOException {
        List<String> lines = readAllLine(filePath);
        return String.join(needLinebreak ? "\n" : "", lines);
    }
}
