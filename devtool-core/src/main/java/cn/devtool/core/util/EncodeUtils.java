package cn.devtool.core.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * 封装各种格式的编码解码工具类.
 * 1.Commons-Codec的 hex/base64 编码
 * 2.自制的base62 编码
 * 3.Commons-Lang的xml/html escape
 * 4.JDK提供的URLEncoder
 * @author chentiefeng
 * @created 2019-11-29 13:31
 */
@Slf4j
public class EncodeUtils {
    private EncodeUtils() {
        throw new IllegalStateException("Utility class");
    }
    private static final String ENCODING = "UTF-8";
    private static final char[] BASE62 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();

    /**
     * Hex编码
     * @param input 输入字节数组
     * @return 编码后的字符串
     */
    public static String encodeHex(byte[] input) {
        return new String(Hex.encodeHex(input));
    }

    /**
     * Hex解码.
     * @param input 输入字符串
     * @return 解码后的字节数组
     * @throws DecoderException DecoderException
     */
    public static byte[] decodeHex(String input) throws DecoderException {
        try {
            return Hex.decodeHex(input.toCharArray());
        } catch (DecoderException e) {
            log.error("Hex解码失败", e);
            throw e;
        }
    }

    /**
     *  Base64编码.
     * @param input 输入字节数组
     * @return 编码后的字符串
     */
    public static String encodeBase64(byte[] input) {
        return new String(Base64.encodeBase64(input));
    }

    /**
     * Base64编码.
     * @param input 输入字符串
     * @return 编码后的字符串
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    public static String encodeBase64(String input) throws UnsupportedEncodingException {
        try {
            return new String(Base64.encodeBase64(input.getBytes(ENCODING)));
        } catch (UnsupportedEncodingException e) {
            log.error("Base64加码失败", e);
            throw e;
        }
    }

    /**
     * Base64解码.
     * @param input 输入字符串
     * @return 解码后的字节数组
     */
    public static byte[] decodeBase64(String input) {
        return Base64.decodeBase64(input.getBytes());
    }

    /**
     * Base64解码.
     * @param input 输入字符串
     * @return 解码后的字节数组
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    public static String decodeBase64String(String input) throws UnsupportedEncodingException {
        try {
            return new String(Base64.decodeBase64(input.getBytes()), ENCODING);
        } catch (UnsupportedEncodingException e) {
            log.error("Base64解码失败", e);
            throw e;
        }
    }

    /**
     * Base62编码。
     * @param input 输入字节数组
     * @return 编码后的字符串
     */
    public static String encodeBase62(byte[] input) {
        char[] chars = new char[input.length];
        for (int i = 0; i < input.length; i++) {
            chars[i] = BASE62[((input[i] & 0xFF) % BASE62.length)];
        }
        return new String(chars);
    }

    /**
     * URL 编码, Encode默认为UTF-8.
     * @param value url值
     * @return 编码后的URL字符串
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    public static String encodeUrl(String value) throws UnsupportedEncodingException {
        try {
            return URLEncoder.encode(value, ENCODING);
        } catch (UnsupportedEncodingException e) {
            log.error("Url编码失败", e);
            throw e;
        }
    }

    /**
     *  URL 解码, Encode默认为UTF-8.
     * @param value url值
     * @return 解码后的URL字符串
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    public static String decodeUrl(String value) throws UnsupportedEncodingException {
        try {
            return URLDecoder.decode(value, ENCODING);
        } catch (UnsupportedEncodingException e) {
            log.error("Url解码失败", e);
            throw e;
        }
    }

}
