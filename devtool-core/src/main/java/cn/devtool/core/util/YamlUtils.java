package cn.devtool.core.util;

import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

/**
 * Yaml文件工具类
 *
 * @author chentiefeng
 * @created 2021/2/8 17:06
 */
public class YamlUtils {
    private YamlUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 读取yaml文件为Map集合
     * @param ymlFile ymlFile
     * @return 返回值集合
     * @throws FileNotFoundException FileNotFoundException
     */
    public static Map<String, String> readYaml(String ymlFile) throws FileNotFoundException {
        Yaml yaml = new Yaml();
        Map<String, Object> yamlMap = (Map<String, Object>) yaml.load(new FileInputStream(ymlFile));
        Map<String, String> resultMap = new HashMap<>();
        getYamlValue(resultMap, "", yamlMap);
        return resultMap;
    }

    /**
     * 递归方法，用于获取yaml各个键的值
     *
     * @param resultMap 结果集合
     * @param prefixKey 前缀
     * @param yamlMap yaml集合
     */
    private static void getYamlValue(Map<String, String> resultMap, String prefixKey, Map<String, Object> yamlMap) {
        for (String key : yamlMap.keySet()) {
            Object value = yamlMap.get(key);
            if (value instanceof Map) {
                getYamlValue(resultMap, prefixKey + key + ".", (Map<String, Object>) value);
            } else {
                resultMap.put(prefixKey + key, String.valueOf(value));
            }
        }
    }
}
