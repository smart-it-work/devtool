package cn.devtool.core.util;


import cn.devtool.core.enums.DatePatternType;

import java.util.UUID;

/**
 * Id工具类
 *
 * @author chentiefeng
 * @created 2021/5/20 23:22
 */
public class IdUtils {
    /**
     * 空值UUID字符串
     */
    public static final String EMPTY_UUID = "00000000-0000-0000-0000-000000000000";
    private IdUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 获取空id
     * @return 空值UUID
     */
    public static UUID emptyUuid() {
        return UUID.fromString(EMPTY_UUID);
    }
    /**
     * 通过UUID字符串获取UUID
     * @param uuidStr uuid字符串
     * @return uuid
     */
    public static UUID uuid(String uuidStr) {
        return UUID.fromString(uuidStr);
    }
    /**
     * 返回UUID
     * @return uuid
     */
    public static UUID uuid() {
        return UUID.randomUUID();
    }

    /**
     * 格式化UUID，替换-为空号
     * @return 格式化后的UUID字符串
     */
    public static String formatUuid() {
        return uuid().toString().replace("-","");
    }
    /**
     * uuid转字符串
     * @param id uuid
     * @return uuid字符串
     */
    public static String toStr(UUID id) {
        return id.toString();
    }

    /**
     *  返回 yyyyMMddHHmmss 格式的日期字符串
     * @return yyyyMMddHHmmss 格式的日期字符串
     */
    public static String nowDateStr() {
        return DateUtils.format(DateUtils.nowDate(), DatePatternType.YYYYMMDDHHMMSSSSS);
    }


}
