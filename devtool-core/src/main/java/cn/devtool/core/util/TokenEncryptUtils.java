package cn.devtool.core.util;

/**
 * token编码工具类
 * @author chentiefeng
 */
public class TokenEncryptUtils {
    private TokenEncryptUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 编码密码,可自定义
     */
    private static final String ENCODED_PASSWORD = "jdp";

    /**
     * 编码
     * @param value 值
     * @return 加密后的字符串
     */
    public static String encoded(String value) {
        return strToHex(encodedString(value, ENCODED_PASSWORD));
    }

    /**
     * 转换
     * @param value 值
     * @param password 密码
     * @return 加密后的字符串
     */
    private static String encodedString(String value, String password) {
        char[] pwd = password.toCharArray();
        int pwdLen = pwd.length;

        char[] strArray = value.toCharArray();
        for (int i=0; i<strArray.length; i++) {
            strArray[i] = (char)(strArray[i] ^ pwd[i%pwdLen] ^ pwdLen);
        }
        return new String(strArray);
    }

    private static String strToHex(String s) {
        return bytesToHexStr(s.getBytes());
    }

    private static String bytesToHexStr(byte[] bytesArray) {
        StringBuilder builder = new StringBuilder();
        String hexStr;
        for (byte bt : bytesArray) {
            hexStr = Integer.toHexString(bt & 0xFF);
            if (hexStr.length() == 1) {
                builder.append("0");
                builder.append(hexStr);
            }else{
                builder.append(hexStr);
            }
        }
        return builder.toString();
    }

    /**
     * 解码
     * @param value 值
     * @return 解码后的值
     */
    public static String decoded(String value) {
        String hexStr = null;
        try {
            hexStr = hexStrToStr(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (hexStr != null) {
            hexStr = encodedString(hexStr, ENCODED_PASSWORD);
        }
        return hexStr;
    }

    private static String hexStrToStr(String hexStr) {
        return new String(hexStrToBytes(hexStr));
    }

    private static byte[] hexStrToBytes(String hexStr) {
        String hex;
        int val;
        byte[] btHexStr = new byte[hexStr.length()/2];
        for (int i=0; i<btHexStr.length; i++) {
            hex = hexStr.substring(2*i, 2*i+2);
            val = Integer.valueOf(hex, 16);
            btHexStr[i] = (byte) val;
        }
        return btHexStr;
    }

}
