package cn.devtool.core.util;

/**
 * 字符工具类
 *
 * @author chentiefeng
 * @created 2021/5/20 23:44
 */
public class CharUtils {
    private CharUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 判断是否时小写字符
     * @param c 字符
     * @return 判断结果
     */
    public static boolean isLowerCase(char c) {
        return (c >= 'a') && (c <= 'z');
    }

    /**
     * 判断是否时大写字符
     * @param c 字符
     * @return 判断结果
     */
    public static boolean isUpperCase(char c) {
        return (c >= 'A') && (c <= 'Z');
    }

    /**
     * 转换字符串为大写字符
     * @param c 字符
     * @return 大写字符
     */
    public static char toUpperCase(char c) {
        return isLowerCase(c) ? (char) (c & 0x5f) : c;
    }

    /**
     * 转换字符串为小写字符
     * @param c 字符
     * @return 小写字符
     */
    public static char toLowerCase(char c) {
        return isUpperCase(c) ? (char) (c ^ 0x20) : c;
    }
}
