package cn.devtool.core.util;

import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 字符串工具类
 *
 * @author chentiefeng
 * @created 2021/7/8 15:03
 */
@Slf4j
public class StringUtils {
    private StringUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 字符串分割字符
     */
    public static final String SPLIT_CHAR = "_";

    /**
     * 判断对象是否为空或长度为0、空格字符串
     * @param value 字符串内容
     * @return 是否为空或长度为0、空格字符串
     */
    public static boolean isBlank(String value) {
        return Objects.isNull(value) || value.trim().length() == 0;
    }

    /**
     * 判断对象是否不为空或长度为0、空格字符串
     * @param value 字符串内容
     * @return 是否不为空或长度为0、空格字符串
     */
    public static boolean nonBlank(String value) {
        return !isBlank(value);
    }

    /**
     * 如果字符串为空，返回默认值；否则返回当前值
     * @param value 值
     * @param defaultValue 默认值
     * @return 默认值或当前值
     */
    public static String getOrDefault(String value, String defaultValue) {
        if (isBlank(value)) {
            return defaultValue;
        }
        return value;
    }

    /**
     * 将连续的多空格换为单空格, 如：在sql语句中用到
     * @param value 字符串
     * @return 空格替换后的字符串
     */
    public static String replaceMoreSpace(String value) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < value.length() - 1; i++) {
            //空格转成int型代表数字是32
            if ((int) value.charAt(i) == 32 && (int) value.charAt(i + 1) == 32) {
                continue;
            }
            stringBuilder.append(value.charAt(i));
        }
        return stringBuilder.toString();
    }

    /**
     * 格式化sql语句，去除换行符和多余的空格
     * @param sql 原始sql
     * @return 格式化的sql
     */
    public static String formatSql(String sql) {
        return  replaceMoreSpace(sql.replace("\n"," "));
    }

    /**
     * 集合 join成字符串
     * @param collection 集合
     * @param delimiter 元素间的分隔符
     * @return join后的字符串
     */
    public static String join(Collection<Object> collection, CharSequence delimiter) {
        return collection.stream().map(String::valueOf).collect(Collectors.joining(delimiter));
    }

    /**
     * 下划线 转换为 驼峰字命名
     * @param underScore 下划线，如：company_code
     * @return 驼峰命名 如：companyCode
     */
    public static String underscoreToLowerCamel(String underScore) {
        StringBuilder out = new StringBuilder();
        String[] wordArr = underScore.split(SPLIT_CHAR);
        out.append(wordArr[0]);
        if (wordArr.length > 1) {
            for (int i=1; i<wordArr.length; i++) {
                out.append(upperFirst(wordArr[i]));
            }
        }
        return out.toString();
    }

    /**
     * 驼峰字命名 转换为 下划线
     * @param lowerCamel 驼峰命名 如：companyCode
     * @return 下划线，如：company_code
     */
    public static String lowerCamelToUnderscore(String lowerCamel) {
        StringBuilder out = new StringBuilder();
        out.append(CharUtils.toLowerCase(lowerCamel.charAt(0)));
        for (int i=1; i<lowerCamel.length(); i++) {
            char tmpChar = lowerCamel.charAt(i);
            out.append(CharUtils.isLowerCase(tmpChar) ? tmpChar : SPLIT_CHAR + CharUtils.toLowerCase(tmpChar));
        }
        return out.toString();
    }

    /**
     * 字符串首字母大写
     * @param str 字符串
     * @return 首字母大写字符串
     */
    public static String upperFirst(String str) {
        return str.substring(0, 1).toUpperCase().concat(str.substring(1));
    }


    /**
     * url追加参数
     *
     * @param url   传入的url
     * @param name  参数名
     * @param value 参数值
     * @return String
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    public static String appendUriParam(String url, String name, String value) throws UnsupportedEncodingException {
        url += (url.indexOf('?') == -1 ? '?' : '&');
        try {
            url += EncodeUtils.encodeUrl(name) + '=' + EncodeUtils.encodeUrl(value);
            return url;
        } catch (UnsupportedEncodingException e) {
            log.error("Url编码失败", e);
            throw e;
        }

    }

    /**
     * 组装新的URL
     *
     * @param url url路径
     * @param params 参数集合
     * @return String
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    public static String appendUriParam(String url, Map<String, String> params) throws UnsupportedEncodingException {
        for (Map.Entry<String, String> entry : params.entrySet()) {
            try {
                url = appendUriParam(url, entry.getKey(), entry.getValue());
            } catch (UnsupportedEncodingException e) {
                log.error("Url编码失败", e);
                throw e;
            }
        }
        return url;
    }

    public static boolean isEmpty(CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

    /**
     * 匿名手机号
     *
     * @param mobile 手机号
     * @return 匿名手机号，如：152****4799
     */
    public static String formatMobile(String mobile) {
        if (isEmpty(mobile)) {
            return null;
        }
        return mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
    }

    /**
     * 匿名银行卡号
     *
     * @param bankCard 银行卡号
     * @return 匿名银行卡号
     */
    public static String formatBankCard(String bankCard) {
        if (isEmpty(bankCard)) {
            return null;
        }
        return bankCard.replaceAll("(\\d{5})\\d{5}\\d{2}(\\d{4})", "$1****$2");
    }

    /**
     * 匿名身份证
     *
     * @param idCard 身份证号
     * @return 匿名身份证号,如4304*****7733
     */
    public static String formatIdCard(String idCard) {

        if (isEmpty(idCard)) {
            return null;
        }
        return idCard.replaceAll("(\\d{4})\\d{10}(\\w{4})", "$1*****$2");
    }

    /**
     * 检测是否为手机号
     * 中国电信号段
     * 133、149、153、173、177、180、181、189、199
     * 中国联通号段
     * 130、131、132、145、155、156、166、175、176、185、186
     * 中国移动号段
     * 134(0-8)、135、136、137、138、139、147、150、151、152、157、158、159、178、182、183、184、187、188、198
     * 其他号段
     * 14号段以前为上网卡专属号段，如中国联通的是145，中国移动的是147等等。
     * 虚拟运营商
     * 电信：1700、1701、1702
     * 移动：1703、1705、1706
     * 联通：1704、1707、1708、1709、171
     *
     * @param mobile 手机号
     * @return 是否为手机号
     */
    public static boolean matchMobile(String mobile) {
        if (mobile == null) {
            return false;
        }
        String regex = "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\\d{8}$";
        return Pattern.matches(regex, mobile);
    }

    /**
     * 检测Email
     *
     * @param email 邮箱
     * @return 是否为Email
     */
    public static boolean matchEmail(String email) {
        if (email == null) {
            return false;
        }
        String regex = "\\w+@\\w+\\.[a-z]+(\\.[a-z]+)?";
        return Pattern.matches(regex, email);
    }


    /**
     * 检测域名
     * @param domain 域名
     * @return 是否为域名
     */
    public static boolean matchDomain(String domain) {
        if (domain == null) {
            return false;
        }
        String regex = "^(?=^.{3,255}$)[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+$";
        return Pattern.matches(regex, domain);
    }

    /**
     * 检测IP
     * @param ip IP
     * @return 是否为IP
     */
    public static boolean matchIp(String ip) {
        if (ip == null) {
            return false;
        }
        String regex = "^(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])$";
        return Pattern.matches(regex, ip);
    }

    /**
     * 检测HttpUrl
     * @param url Url
     * @return 是否为 HttpUrl
     */
    public static boolean matchHttpUrl(String url) {
        if (url == null) {
            return false;
        }
        String regex = "^(?=^.{3,255}$)(http(s)?:\\/\\/)?(www\\.)?[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+(:\\d+)*(\\/\\w+\\.\\w+)*([\\?&]\\w+=\\w*)*$";
        return Pattern.matches(regex, url);
    }

    /**
     * 校验银行卡卡号
     * 校验过程：
     * 1、从卡号最后一位数字开始，逆向将奇数位(1、3、5等等)相加。
     * 2、从卡号最后一位数字开始，逆向将偶数位数字，先乘以2（如果乘积为两位数，将个位十位数字相加，即将其减去9），再求和。
     * 3、将奇数位总和加上偶数位总和，结果应该可以被10整除。
     * @param bankCard 银行卡号
     * @return 是否为银行卡号
     */
    public static boolean matchBankCard(String bankCard) {
        if (bankCard == null) {
            return false;
        }
        if (bankCard.length() < 15 || bankCard.length() > 19) {
            return false;
        }
        char bit = getBankCardCheckCode(bankCard.substring(0, bankCard.length() - 1));
        if (bit == 'N') {
            return false;
        }
        return bankCard.charAt(bankCard.length() - 1) == bit;
    }

    /**
     * 从不含校验位的银行卡卡号采用 Luhm 校验算法获得校验位
     *
     * @param nonCheckCodeBankCard 不含校验位的银行卡卡号
     * @return 校验位
     */
    public static char getBankCardCheckCode(String nonCheckCodeBankCard) {
        if (nonCheckCodeBankCard == null || nonCheckCodeBankCard.trim().length() == 0
                || !nonCheckCodeBankCard.matches("\\d+")) {
            //如果传的不是数据返回N
            return 'N';
        }
        char[] chs = nonCheckCodeBankCard.trim().toCharArray();
        int luhmSum = 0;
        for (int i = chs.length - 1, j = 0; i >= 0; i--, j++) {
            int k = chs[i] - '0';
            if (j % 2 == 0) {
                k *= 2;
                k = k / 10 + k % 10;
            }
            luhmSum += k;
        }
        return (luhmSum % 10 == 0) ? '0' : (char) ((10 - luhmSum % 10) + '0');
    }

    /**
     * 校验密码强度
     * @param passwordStr 密码字符串
     * @return Z = 字母 S = 数字 T = 特殊字符
     */
    public static String checkPassword(String passwordStr) {
        String regexZ = "\\d*";
        String regexS = "[a-zA-Z]+";
        String regexT = "\\W+$";
        String regexZT = "\\D*";
        String regexST = "[\\d\\W]*";
        String regexZS = "\\w*";
        String regexZST = "[\\w\\W]*";

        if (passwordStr.matches(regexZ)) {
            return "弱";
        }
        if (passwordStr.matches(regexS)) {
            return "弱";
        }
        if (passwordStr.matches(regexT)) {
            return "弱";
        }
        if (passwordStr.matches(regexZT)) {
            return "中";
        }
        if (passwordStr.matches(regexST)) {
            return "中";
        }
        if (passwordStr.matches(regexZS)) {
            return "中";
        }
        if (passwordStr.matches(regexZST)) {
            return "强";
        }
        return passwordStr;
    }

    /**
     * 将 Exception 转化为 String
     * @param e 异常
     * @return 异常字符串
     */
    public static String getExceptionToString(Throwable e) {
        if (Objects.isNull(e)){
            return "";
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter, true);
        e.printStackTrace(printWriter);
        printWriter.flush();
        stringWriter.flush();
        return stringWriter.toString();
    }
}
