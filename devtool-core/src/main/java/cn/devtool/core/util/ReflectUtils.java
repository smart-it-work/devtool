package cn.devtool.core.util;


import java.lang.reflect.Field;

/**
 * 发射工具类
 *
 * @author chentiefeng
 * @created 2021/7/14 14:02
 */
public class ReflectUtils {
    private ReflectUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 获取某个对象某个属性的值
     * @param field 属性
     * @param target 对象
     * @return 属性的值
     */
    public static Object getField(Field field, Object target) {
        field.setAccessible(true);
        try {
            return field.get(target);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
