package cn.devtool.core.util;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.util.Random;

/**
 * 验证码工具类
 *
 * @author chentiefeng
 * @created 2021/7/13 12:17
 */
public class CaptchaUtil {
    private CaptchaUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 创建验证码图片
     * @param width 长度
     * @param height 高度
     * @param codeLength 字符个数
     * @return 验证码信息对象
     */
    public static CaptchaInfo createCaptcha(int width, int height, int codeLength) {
        //1.  创建一个对象，在内存中画图像(验证码图片对象)
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        //2. 美化图片
        //填充背景色
        Graphics g = image.getGraphics(); //画笔对象
        g.setColor(Color.lightGray);  //画笔颜色
        g.fillRect(0, 0, width, height); //填充颜色
        //画边框
        g.setColor(Color.blue);
        g.drawRect(0, 0, width - 1, height - 1);
        //写验证码
        String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        //生成随机数
        Random random = new Random();
        StringBuilder code = new StringBuilder();
        for (int n = 1; n < codeLength; n++) {
            int i = random.nextInt(str.length());
            char c = str.charAt(i);
            code.append(c);
            g.drawString(c + "", width / 5 * n, 3 * height / 5);
        }
        //画干扰线
        g.setColor(Color.GREEN);
        for (int n = 1; n < 7; n++) {
            //随机生成四个坐标点
            int randomWidth1 = random.nextInt(width);
            int randomWidth2 = random.nextInt(width);
            int randomHeight1 = random.nextInt(height);
            int randomHeight2 = random.nextInt(height);
            g.drawLine(randomWidth1,randomHeight1,randomWidth2,randomHeight2);
        }
        return new CaptchaInfo(code.toString(), image);
    }

    @Data
    @AllArgsConstructor
    public static class CaptchaInfo {
        /**
         * 验证码
         */
        private String code;
        /**
         * 验证码图片
         */
        private RenderedImage image;
    }
}
