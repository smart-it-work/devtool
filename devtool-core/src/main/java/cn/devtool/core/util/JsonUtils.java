package cn.devtool.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

/**
 * Json工具类
 *
 * @author chentiefeng
 * @created 2021/7/8 14:50
 */
@Slf4j
public class JsonUtils {
    private JsonUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 对象转换为json字符串
     * @param obj 对象
     * @return 出现异常时，返回null,否则，返回Json字符串
     * @throws JsonProcessingException JONS处理异常
     */
    public static String toJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error("对象转换json出现异常", e);
            throw e;
        }
    }

    /**
     * 解析Json字符串为对象
     * @param jsonString json字符串
     * @param targetType 对象类
     * @param <T> 对象类型
     * @return 出现异常时，返回null,否则，返回对象
     * @throws IOException IO异常
     */
    public static <T> T toObject(String jsonString, Class<T> targetType) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(jsonString, targetType);
        } catch (IOException e) {
            log.error("解析Json字符串为对象出现异常", e);
            throw e;
        }
    }

    /**
     * 解析Json文件为对象
     * @param jsonFile json文件
     * @param targetType  对象类
     * @param <T>  对象类型
     * @return 出现异常时，返回null,否则，返回对象
     * @throws IOException IO异常
     */
    public static <T> T toObject(File jsonFile, Class<T> targetType) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(jsonFile, targetType);
        } catch (IOException e) {
            log.error("解析Json文件为对象出现异常", e);
            throw e;
        }
    }

}
