package cn.devtool.core.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.StringTokenizer;
import java.util.function.BinaryOperator;

/**
 * 数学工具类
 *
 * @author chentiefeng
 * @created 2021/7/12 10:36
 */
public class MathUtils {
    private MathUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 科学计数法包含的字符
     */
    public static final String SCIENTIFIC_CHARACTER = "E";
    /**
     * 便于阅读的字符
     */
    public static final String READ_CHARACTER = ",";

    /**
     * 提供精确加法计算的add方法
     * @param value1 被加数
     * @param value2 加数
     * @return 两个参数的和
     */
    public static double add(double value1,double value2){
        return calc(BigDecimal::add, BigDecimal.valueOf(value1), BigDecimal.valueOf(value2)).doubleValue();
    }

    /**
     * 提供精确减法运算的sub方法
     * @param value1 被减数
     * @param value2 减数
     * @return 两个参数的差
     */
    public static double subtract(double value1,double value2){
        return calc(BigDecimal::subtract, BigDecimal.valueOf(value1), BigDecimal.valueOf(value2)).doubleValue();
    }

    /**
     * 提供精确乘法运算的mul方法
     * @param value1 被乘数
     * @param value2 乘数
     * @return 两个参数的积
     */
    public static double multiply(double value1,double value2){
        return calc(BigDecimal::multiply, BigDecimal.valueOf(value1), BigDecimal.valueOf(value2)).doubleValue();
    }

    /**
     * 提供精确的除法运算方法div
     * @param value1 被除数
     * @param value2 除数
     * @param scale 精确范围 (四舍五入)
     * @return 两个参数的商
     * @throws IllegalAccessException IllegalAccessException
     */
    public static double divide(double value1,double value2,int scale) throws IllegalAccessException{
        //如果精确范围小于0，抛出异常信息
        if(scale<0){
            throw new IllegalAccessException("精确度不能小于0");
        }
        return calc((val1, val2) -> val1.divide(val2, scale, RoundingMode.HALF_UP), BigDecimal.valueOf(value1), BigDecimal.valueOf(value2)).doubleValue();
    }

    /**
     * 计算
     * @param calcFun 计算方法
     * @param value1 参数1
     * @param value2 参数2
     * @return 计算结果
     */
    public static BigDecimal calc(BinaryOperator<BigDecimal> calcFun, BigDecimal value1, BigDecimal value2) {
        return calcFun.apply(value1, value2);
    }

    /**
     * 保留几位小数（四舍五入）
     * @param value 值
     * @param newScale 小数位数
     * @return 保留几位小数后的值
     */
    public static double scale(BigDecimal value, int newScale) {
        return value.setScale(newScale, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * 格式化为货币格式, 如：￥150.48
     * @param value 值
     * @return 货币格式值
     */
    public static String formatCurrency(BigDecimal value) {
        //建立货币格式化引用
        return  NumberFormat.getCurrencyInstance().format(value);
    }

    /**
     * 格式化为百分比格式，如： 0.8%
     * @param value 值
     * @return 百分比格式值
     */
    public static String formatPercent(BigDecimal value) {
        //建立百分比格式化引用
        NumberFormat percent = NumberFormat.getPercentInstance();
        //百分比小数点最多4位
        percent.setMaximumFractionDigits(4);
        return percent.format(value);
    }

    /**
     * 格式化数字， 如果含有逗号，去掉；如果含有科学计数法，转换为平常值
     * @param value 字符串数值
     * @return 格式化后的数字
     */
    public static BigDecimal format(String value) {
        if (StringUtils.isBlank(value)) {
            return BigDecimal.ZERO;
        }
        if (value.contains(SCIENTIFIC_CHARACTER)) {
            // 科学计数法
            return new BigDecimal(new BigDecimal(value).toPlainString());
        }
        if (value.contains(READ_CHARACTER)) {
            // 含有逗号
            java.util.StringTokenizer st = new StringTokenizer( value, READ_CHARACTER);
            StringBuilder sb = new StringBuilder();
            while(st.hasMoreTokens())   {
                sb.append(st.nextToken());
            }
            return new BigDecimal(sb.toString());
        }
        return new BigDecimal(value);
    }
}
