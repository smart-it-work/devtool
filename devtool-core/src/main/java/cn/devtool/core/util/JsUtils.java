package cn.devtool.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import lombok.extern.slf4j.Slf4j;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Js工具类，主要用于一些条件
 *
 * @author chentiefeng
 * @created 2021/6/21 10:58
 */
@Slf4j
public class JsUtils {
    private JsUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 返回 Predicate判断
     * @param conditions 判断条件
     * @param <T> 参数对象类型
     * @param defaultPredicate 当使用js的判断条件出现异常时，使用Predicate判断
     * @return Predicate判断
     */
    public static <T> Predicate<T> createPredicateByConditions(String conditions, Predicate<T> defaultPredicate) {
        try {
            ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
            String functionStr = String.format("function(data){ return %s }", conditions);
            return (Predicate<T>)engine.eval(String.format("new java.util.function.Predicate(%s)", functionStr));
        } catch (Exception e) {
            log.error("js转换判断条件[{}]为Predicate对象出现异常，使用defaultPredicate[{}]", conditions, defaultPredicate, e);
            return defaultPredicate;
        }
    }

    /**
     * 获取Json字符串中的值
     * @param jsonData json字符串,
     * @param dataFunction 取值逻辑的js方法内容
     * @return json字符串中的值,
     * @throws JsonProcessingException JsonProcessingException
     */
    public static String getJsonValue(String jsonData, String dataFunction) throws JsonProcessingException {
        Function<String, Object> dataConvertFunction = createFunction("data = eval('('+data+')');\n" + dataFunction);
        Object result = dataConvertFunction.apply(jsonData);
        if(((ScriptObjectMirror) result).isArray()) {
            return JsonUtils.toJson(((ScriptObjectMirror) result).values());
        }
        return JsonUtils.toJson(result);
    }

    /**
     * 通过js方法内容，格式化为js方法
     * @param functionStr js方法内容
     * @param <T> 值参数类型
     * @return 返回 Function
     */
    private static <T> Function<T, Object> createFunction(String functionStr) {
        try {
            ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
            functionStr = String.format("function(data){ %s }", functionStr);
            return (Function<T, Object>)engine.eval(String.format("new java.util.function.Function(%s)", functionStr));
        } catch (Exception e) {
            log.error("js转换方法内容[{}]为Predicate对象出现异常，返回默认值null", functionStr, e);
            return data -> null;
        }
    }
}
