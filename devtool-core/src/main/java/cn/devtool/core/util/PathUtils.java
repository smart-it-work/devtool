package cn.devtool.core.util;

import java.net.URL;
import java.util.Objects;

/**
 * 路径工具类
 *
 * @author chentiefeng
 * @created 2021/6/11 18:13
 */
public class PathUtils {
    private PathUtils() {
        throw new IllegalStateException("Utility class");
    }
    public static String getClassPath() {
        URL url = PathUtils.class.getResource("/");
        return Objects.isNull(url) ? "": url.getPath();
    }
}
