package cn.devtool.core.util;

import cn.devtool.core.enums.DatePatternType;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MONTHS;

/**
 * 日期工具类
 *
 * @author chentiefeng
 * @created 2021/5/20 22:07
 */
@Slf4j
public class DateUtils {
    /**
     * 日期分隔符
     */
    public static final String DATE_SPLIT = "-";

    private DateUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 获取当前日期
     * @return 当前日期
     */
    public static LocalDate nowDate() {
        return LocalDate.now();
    }

    /**
     * 获取当前时间
     * @return 当前时间
     */
    public static LocalDateTime now() {
        return LocalDateTime.now();
    }

    /**
     * 计算两个日期之间相差的月数
     * @param start 开始日期
     * @param end 结束日期
     * @return 两个日期之间相差的月数，如果为负值，代表开始时间大于结束时间，可以取绝对值
     */
    public static int deltaMonth(YearMonth start, YearMonth end) {
        return (end.getYear() - start.getYear()) * 12 + (end.getMonthValue() - start.getMonthValue());
    }

    /**
     * 日期几个月后的当前日期
     * @param date 当前日期
     * @param month 月数
     * @return 几个月后的当前日期
     */
    public static LocalDate offsetMonth(LocalDate date, int month) {
        return date.plusMonths(month);
    }

    /**
     * 解析日期时间
     * @param dateTime 日期时间
     * @param patternType 日期格式
     * @return 解析后的日期时间
     */
    public static LocalDateTime parseLocalDateTime(String dateTime, DatePatternType patternType) {
        DateTimeFormatter dateTimeFormatter = getDateTimeFormatter(patternType);
        return LocalDateTime.parse(dateTime, dateTimeFormatter);
    }

    /**
     * 解析日期时间
     * @param dateTime 日期时间
     * @param patternType 日期格式
     * @return 解析后的日期
     */
    public static Date parseDate(String dateTime, DatePatternType patternType) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(patternType.getValue());
        try {
            return simpleDateFormat.parse(dateTime);
        } catch (ParseException e) {
            log.error("date parse get Exception:", e);
            return  null;
        }
    }

    /**
     * 解析日期时间
     * @param dateTime 日期时间
     * @param patternType 日期格式
     * @return 日期
     */
    public static LocalDate parseLocalDate(String dateTime, DatePatternType patternType) {
        return LocalDate.parse(dateTime, getDateTimeFormatter(patternType));
    }
    /**
     * 解析日期
     * @param date 日期，如：20201022或者2020-10-22
     * @return 解析后的日期
     */
    public static LocalDate parseDate(String date) {
        DatePatternType patternType = DatePatternType.YYYY_MM_DD;
        if (!date.contains(DATE_SPLIT)) {
            patternType = DatePatternType.YYYYMMDD;
        }
        return LocalDate.parse(date, getDateTimeFormatter(patternType));
    }

    /**
     * 获取两个日期之间的所有日期集合(yyyy-MM-dd格式)
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @return  中间的日期集合（yyyy-MM-dd格式）
     */
    public static List<String> getFormatDays(LocalDate startDate, LocalDate endDate) {
        List<LocalDate> resultDays = getDays(startDate, endDate);
        return resultDays.stream().map(item -> item.format(getDateTimeFormatter(DatePatternType.YYYY_MM_DD))).collect(Collectors.toList());
    }
    /**
     * 获取两个日期之间的所有日期集合
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @return 中间的日期集合
     */
    public static List<LocalDate> getDays(LocalDate startDate, LocalDate endDate) {
        long days = DAYS.between(startDate, endDate) + 1;
        List<LocalDate> resultDays = new ArrayList<>((int) days);
        for (int i = 0; i < days; i++) {
            resultDays.add(startDate.plusDays(i));
        }
        return resultDays;
    }
    /**
     * 获取两个日期之间的天数
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @return 两个日期之间的天数
     */
    public static long betweenDays(LocalDate startDate, LocalDate endDate) {
        return DAYS.between(startDate, endDate);
    }

    /**
     * 获取两个日期之间的月数
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @return 两个日期之间的月数
     */
    public static long betweenMonths(LocalDate startDate, LocalDate endDate) {
        return MONTHS.between(startDate, endDate);
    }

    /**
     * 获取date当月的第一天日期的字符串，格式是：yyyy-MM-dd
     * @param date 日期
     * @return 月的第一天日期的字符串，格式是：yyyy-MM-dd
     */
    public static String getMonthFirstDayStr(LocalDate date) {
        return getMonthFirstDay(date).format(getDateTimeFormatter(DatePatternType.YYYY_MM_DD));
    }
    /**
     * 获取date当月的第一天日期
     * @param date 日期
     * @return 当月的第一天日期
     */
    public static LocalDate getMonthFirstDay(LocalDate date) {
        return date.with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取date当月的最后一天日期的字符串，格式是：yyyy-MM-dd
     * @param date 日期
     * @return 当月的最后一天日期的字符串，格式是：yyyy-MM-dd
     */
    public static String getMonthLastDayStr(LocalDate date) {
        return getMonthLastDay(date).format(getDateTimeFormatter(DatePatternType.YYYY_MM_DD));
    }

    /**
     * 获取date当月的最后一天日期
     * @param date 日期
     * @return 当月的最后一天日期
     */
    public static LocalDate getMonthLastDay(LocalDate date) {
        return date.with(TemporalAdjusters.lastDayOfMonth());
    }


    /**
     * 获取date下月的第一天日期的字符串，格式是：yyyy-MM-dd
     * @param date 日期
     * @return 下月的第一天日期的字符串，格式是：yyyy-MM-dd
     */
    public static String getNextMonthFirstDayStr(LocalDate date) {
        return getNextMonthFirstDay(date).format(getDateTimeFormatter(DatePatternType.YYYY_MM_DD));
    }
    /**
     * 获取date下月的第一天日期
     * @param date 日期
     * @return 下月的第一天日期
     */
    public static LocalDate getNextMonthFirstDay(LocalDate date) {
        return date.plusMonths(1).with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取date上个月的最后一天日期的字符串，格式是：yyyy-MM-dd
     * @param date 日期
     * @return 上个月的最后一天日期的字符串，格式是：yyyy-MM-dd
     */
    public static String getLastMonthLastDayStr(LocalDate date) {
        return getLastMonthLastDay(date).format(getDateTimeFormatter(DatePatternType.YYYY_MM_DD));
    }
    /**
     * 获取date上个月的最后一天日期
     * @param date 日期
     * @return 上个月的最后一天日期
     */
    public static LocalDate getLastMonthLastDay(LocalDate date) {
        return date.minusMonths(1).with(TemporalAdjusters.lastDayOfMonth());
    }
    /**
     * 格式化日期为yyyy-MM-dd 格式的字符串
     * @param date 日期
     * @return 格式后的字符串yyyy-MM-dd
     */
    public static String format(LocalDate date) {
        return format(date, DatePatternType.YYYY_MM_DD);
    }

    /**
     * 格式化日期为字符串
     * @param date 日期
     * @param datePatternType 格式 ${@link DatePatternType}
     * @return 格式后的字符串
     */
    public static String format(LocalDate date, DatePatternType datePatternType) {
        return date.format(getDateTimeFormatter(datePatternType));
    }

    /**
     * 格式化日期为字符串
     * @param date 日期
     * @param datePattern 日期格式
     * @return 格式后的字符串
     */
    public static String format(LocalDate date, String datePattern) {
        return date.format(getDateTimeFormatter(datePattern));
    }

    /**
     * 1天后的当前时间
     * @param date 日期
     * @return 1天后的当前时间
     */
    public static Date dateAfterDay(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }

    /**
     * 格式化日期，
     * @param date 日期
     * @return YYYY_MM_DD_HH_MM_SS格式的日期字符串，当日期为空时，返回空字符串
     */
    public static String formatDate(Date date) {
        if (null == date) {
            return "";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DatePatternType.YYYY_MM_DD_HH_MM_SS.getValue());
        return simpleDateFormat.format(date);
    }
    /**
     * 根据格式，获取格式化对象
     * @param pattern 格式化格式，如：yyyy-MM-dd
     * @return 日期格式化类
     */
    public static DateTimeFormatter getDateTimeFormatter(String pattern) {
        return DateTimeFormatter.ofPattern(pattern);
    }



    /**
     * 根据格式，获取格式化对象
     * @param patternType 格式 ${@link DatePatternType}
     * @return 日期格式化类
     */
    public static DateTimeFormatter  getDateTimeFormatter(DatePatternType patternType) {
        return  getDateTimeFormatter(patternType.getValue());
    }

    /**
     * LocalDateTime转换为Date
     * @param localDateTime LocalDateTime
     * @return Date
     */
    public static Date convertToDate(LocalDateTime localDateTime) {
        ZoneId zoneId = getSystemZoneId();
        ZonedDateTime zdt = localDateTime.atZone(zoneId);
        return Date.from(zdt.toInstant());
    }

    /**
     * 当月的第一天
     * @param yearMonth 年月
     * @return 返回第一天
     */
    public static String toDateStringFromYearMonth(YearMonth yearMonth) {
        LocalDate date = LocalDate.of(yearMonth.getYear(), yearMonth.getMonth(), 1);
        return format(date, DatePatternType.YYYY_MM_DD);
    }

    /**
     * Date转换为LocalDateTime
     * @param date Date
     * @return LocalDateTime
     */
    public static LocalDateTime convertToLocalDateTime(Date date) {
        Instant instant = date.toInstant();
        ZoneId zoneId = getSystemZoneId();
        return  instant.atZone(zoneId).toLocalDateTime();
    }

    private static ZoneId getSystemZoneId() {
        return ZoneId.of("UTC+8");
    }
}
