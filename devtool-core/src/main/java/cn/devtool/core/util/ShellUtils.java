package cn.devtool.core.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Shell工具类
 * @author chentiefeng
 * @created 2021/7/14 16:38
 */
public class ShellUtils {
    public static void main(String[] args) throws IOException {
        Process process = null;
        try {
            //File tmpFile = File.createTempFile("test", ".csv");
            String zipFileName = "/tmp" + File.separator + "tesst" + ".zip";
            String[] cmds = {"/bin/bash", "-c", "zip -j " + zipFileName + " " + StringUtils.join(Arrays.asList("test1546002628256460403.csv"), " ")};
            process = new ProcessBuilder(cmds).redirectErrorStream(true).start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        InputStream in = null;
        try {
            in = process.getInputStream();
            // 从返回的内容中读取所需内容
            final BufferedReader bReader = new BufferedReader(new InputStreamReader(in));
            String line = null;
            StringBuilder result = new StringBuilder();
            try {
                while ((line = bReader.readLine()) != null) {
                    result.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(result.toString());
        } catch (Exception e){
            e.printStackTrace();
        }
        finally {
            in.close();
            process.destroy();
        }
    }
}
