package cn.devtool.core.test.util;

import cn.devtool.core.util.TokenUtils;

/**
 * @author chentiefeng
 * @created 2021/7/15 23:31
 */
public class TokenUtilsTest {

    public void test(String[] args) {
        String str = "username_and_password";

        // 获取token
        String token = TokenUtils.getToken(str);
        System.out.println("token Result: " + token);

        // 校验token
        String checkToken = TokenUtils.checkToken(token);
        System.out.println("checkToken Result: " + checkToken);
        if(str.equals(checkToken)) {
            System.out.println("==>token verification succeeded!");
        }

    }

}
