package cn.devtool.core.test.util;

import cn.devtool.core.util.JsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * @author chentiefeng
 * @created 2021/7/15 18:23
 */
public class JsUtilsTest {
    public static void main(String[] args) throws JsonProcessingException {
        String jsonData = "{\"username\":\"chentiefeng\",\"age\":25}";
        System.out.println(JsUtils.getJsonValue(jsonData, " var userInfo = {};\n userInfo[data['username']] = data['age'];\n  return userInfo;"));
    }
}
