package cn.devtool.excel;


import cn.devtool.excel.model.ExcelCell;
import cn.devtool.excel.model.ExcelData;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;

import java.io.File;
import java.io.IOException;

/**
 * Excel测试
 *
 * @author chentiefeng
 * @created 2021/4/22 9:39
 */
public class ExcelTest {
    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\imche\\Desktop", "新建 Microsoft Excel 工作表.xlsx");
        ExcelData excelData = ExcelReader.read(file);
        File file2 = new File("C:\\Users\\imche\\Desktop\\supload", "test3.xlsx");
        ExcelWriter.write(file2, excelData);
        System.out.println(excelData);
    }
}
