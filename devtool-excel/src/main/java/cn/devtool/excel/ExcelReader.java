package cn.devtool.excel;

import cn.devtool.excel.enums.ExcelCellDataType;
import cn.devtool.excel.exception.ExcelException;
import cn.devtool.excel.model.ExcelCell;
import cn.devtool.excel.model.ExcelData;
import cn.devtool.excel.model.ExcelRow;
import cn.devtool.excel.model.ExcelSheet;
import cn.devtool.excel.model.MergedRegionResult;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Excel 读取工具类
 *
 * @author chentiefeng
 * @created 2021/4/21 17:02
 */
public class ExcelReader {
    private ExcelReader() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 读取Excel文件的数据
     * @param file File
     * @return ExcelData
     * @throws IOException IOException
     */
    public static ExcelData read(File file) throws IOException {
        ExcelData excelData = new ExcelData(file.getName());
        Workbook workbook = getWorkbook(excelData.getFileName(), new FileInputStream(file));
        excelData.setSheets(getSheets(workbook));
        return excelData;
    }

    /**
     * 获取Workbook中的Sheet数据
     * @param workbook Workbook
     * @return shell集合
     */
    public static List<ExcelSheet> getSheets(Workbook workbook) {
        List<ExcelSheet> resultSheetList = new ArrayList<>(workbook.getNumberOfSheets());
        Iterator<Sheet> iterator = workbook.sheetIterator();
        while (iterator.hasNext()) {
            Sheet sheet = iterator.next();
            resultSheetList.add(convertSheet(sheet));
        }
        return resultSheetList;
    }

    /**
     * 获取Workbook中的某个SheetSheet数据
     * @param workbook Workbook
     * @param sheetName Sheet名称
     * @return shell
     */
    public static ExcelSheet getSheet(Workbook workbook, String sheetName) {
        Sheet sheet = workbook.getSheet(sheetName);
        return convertSheet(sheet);
    }
    /**
     * 获取Workbook中的某个SheetSheet数据
     * @param workbook Workbook
     * @param sheetIndex Sheet索引
     * @return shell
     */
    public static ExcelSheet getSheet(Workbook workbook, int sheetIndex) {
        Sheet sheet = workbook.getSheetAt(sheetIndex);
        return convertSheet(sheet);
    }
    /**
     * 转换Sheet为ExcelSheet
     * @param sheet Sheet
     * @return ExcelSheet
     */
    private static ExcelSheet convertSheet(Sheet sheet) {
        ExcelSheet excelSheet = new ExcelSheet(sheet.getSheetName());
        excelSheet.setRows(getSheetRows(sheet));
        return excelSheet;
    }

    /**
     * 获取Sheet中的行数据
     * @param sheet Sheet
     * @return ExcelRow集合
     */
    private static List<ExcelRow> getSheetRows(Sheet sheet) {
        List<ExcelRow> excelRowList = new ArrayList<>(sheet.getPhysicalNumberOfRows());
        Iterator<Row> iterator = sheet.iterator();
        int i = 1;
        while (iterator.hasNext()) {
            Row row = iterator.next();
            ExcelRow tmpRow = convertRow(i, row);
            if (!CollectionUtils.isEmpty(tmpRow.getCells())) {
                excelRowList.add(tmpRow);
            }
            i ++;
        }
        return excelRowList;
    }

    /**
     * 转换Row为ExcelRow
     * @param rowNumber  行号，从1开始
     * @param row Row
     * @return ExcelRow
     */
    private static ExcelRow convertRow(int rowNumber, Row row) {
        ExcelRow excelRow = new ExcelRow();
        excelRow.setRowNum(rowNumber);
        excelRow.setHeight(row.getHeight());
        excelRow.setCells(getCells(rowNumber, row));
        return excelRow;
    }

    /**
     * 通过Row获取ExcelColumn集合
     * @param rowNumber  行号
     * @param row Row
     * @return ExcelCell集合
     */
    private static List<ExcelCell> getCells(int rowNumber, Row row) {
        List<ExcelCell> excelCellList = new ArrayList<>(row.getPhysicalNumberOfCells());
        Iterator<Cell> iterator = row.iterator();
        int i = 1;
        while (iterator.hasNext()) {
            Cell cell = iterator.next();
            excelCellList.add(convertCell(rowNumber, i, cell));
            i ++;
        }
        if (isEmptyRow(excelCellList)) {
            return Collections.emptyList();
        }
        return excelCellList;
    }

    /**
     * 是否是空行
     * @param rowCells rowCells
     * @return 是否是空行
     */
    private static boolean isEmptyRow(List<ExcelCell> rowCells) {
        long notEmptyValueCount = rowCells.stream().filter(item -> item.getValue() != null && !String.valueOf(item.getValue()).trim().isEmpty()).count();
        return notEmptyValueCount == 0;
    }

    /**
     * 转换Cell为ExcelCell
     * @param rowNumber 行号
     * @param columnNumber 列号
     * @param cell Cell
     * @return ExcelCell
     */
    private static ExcelCell convertCell(int rowNumber, int columnNumber, Cell cell) {
        ExcelCell excelCell = new ExcelCell();
        excelCell.setDataType(getCellDataType(cell, cell.getCellType()));
        excelCell.setAddress(cell.getAddress());
        if (cell.getCellType() == CellType.FORMULA) {
            excelCell.setCellFormula(cell.getCellFormula());
            excelCell.setFormulaResultDataType(getCellDataType(cell, cell.getCellType()));
        }
        excelCell.setRowNum(rowNumber);
        excelCell.setColumnNum(columnNumber);
        excelCell.setWidth(cell.getSheet().getColumnWidth(columnNumber-1));
        excelCell.setValue(getCellValue(cell, false));
        excelCell.setFormatValue(getCellValue(cell, true));
        return excelCell;
    }

    /**
     * 获取Cell中值类型
     * @param cell Cell
     * @param cellType CellType
     * @return Cell中值类型
     */
    private static ExcelCellDataType getCellDataType(Cell cell, CellType cellType) {
        switch (cellType) {
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    if (cell.getCellStyle().getDataFormat() == 14) {
                        return ExcelCellDataType.DATE;
                    } else if (cell.getCellStyle().getDataFormat() == 21) {
                        return ExcelCellDataType.TIME;
                    } else {
                        return ExcelCellDataType.DATE_TIME;
                    }
                }
                return ExcelCellDataType.NUMBER;
            case STRING:
                return ExcelCellDataType.STRING;
            case BOOLEAN:
                return ExcelCellDataType.BOOLEAN;
            case FORMULA:
                return getCellDataType(cell, cell.getCachedFormulaResultType());
            default:
                break;
        }
        return ExcelCellDataType.DEFAULT;
    }
    /**
     * 获取Cell的值
     * @param cell Cell
     * @param formatFlag 是否格式化
     * @return Cell的值
     */
    private static Object getCellValue(Cell cell, boolean formatFlag) {
        Object result = null;
        MergedRegionResult mergedRegionResult=isMergedRegion(cell);
        if (Boolean.TRUE.equals(mergedRegionResult.getMerged())) {
            CellRangeAddress range=mergedRegionResult.getMergedRegion();
            Row fRow = cell.getSheet().getRow(range.getFirstRow());
            cell = fRow.getCell(range.getFirstColumn());
        }
        switch (cell.getCellType()) {
            case NUMERIC:
                result = getNumberOrDate(cell);
                break;
            case STRING:
                if (!cell.getStringCellValue().isEmpty()) {
                    result = cell.getStringCellValue().trim();
                }
                break;
            case BOOLEAN:
                result = cell.getBooleanCellValue();
                break;
            case FORMULA:
                result = getCachedFormulaValue(cell);
                break;
            default:
                break;
        }
        if (Objects.nonNull(result) && cell.getCellType() != CellType.FORMULA && formatFlag) {
            result = new DataFormatter().formatCellValue(cell);
            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                if (cell.getCellStyle().getDataFormat() == 14) {
                    result = cell.getLocalDateTimeCellValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                } else if (cell.getCellStyle().getDataFormat() == 21) {
                    result = cell.getLocalDateTimeCellValue().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
                } else {
                    result = cell.getLocalDateTimeCellValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                }
            }
        }
        return result;
    }

    /**
     * 获取Formula Cell的值
     * @param formulaCell Formula Cell
     * @return Formula Cell的值
     */
    public static Object getCachedFormulaValue(Cell formulaCell) {
        switch (formulaCell.getCachedFormulaResultType()) {
            case BOOLEAN:
                return formulaCell.getBooleanCellValue();
            case NUMERIC:
                return getNumberOrDate(formulaCell);
            case STRING:
                return formulaCell.getStringCellValue();
            default:
                break;
        }
        return null;
    }

    /**
     * 是否是合并单元格
     * @param cell Cell
     * @return 合并单元格对象
     */
    private static MergedRegionResult isMergedRegion(Cell cell) {
        return isMergedRegion(cell.getSheet(), cell.getRowIndex(), cell.getColumnIndex());
    }
    /**
     * 是否是合并单元格
     * @param sheet Sheet
     * @param row Row
     * @param column 列号
     * @return 合并单元格对象
     */
    private static MergedRegionResult isMergedRegion(Sheet sheet, int row , int column) {
        int sheetMergeCount = sheet.getNumMergedRegions();
        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            int firstColumn = range.getFirstColumn();
            int lastColumn = range.getLastColumn();
            int firstRow = range.getFirstRow();
            int lastRow = range.getLastRow();
            if(row >= firstRow && row <= lastRow && column >= firstColumn && column <= lastColumn){
                return  new MergedRegionResult(true,range);
            }
        }
        return new MergedRegionResult(false);
    }
    /**
     * 获取Number或Date类型的值
     * @param cell Cell
     * @return Cell的值
     */
    private static Object getNumberOrDate(Cell cell) {
        if (DateUtil.isCellDateFormatted(cell)) {
            return cell.getLocalDateTimeCellValue();
        } else {
            return getNumberValue(cell);
        }
    }

    /**
     * 获取数字的值
     * @param cell Cell
     * @return 数字的值
     */
    private static BigDecimal getNumberValue(Cell cell) {
        double doubleValue = cell.getNumericCellValue();
        // 防止出现科学计算法
        NumberFormat numberFormat =NumberFormat.getInstance();
        numberFormat.setGroupingUsed(false);
        return new BigDecimal(numberFormat.format(doubleValue));
    }

    /**
     * 获取Workbook
     * @param fileName 文件名
     * @param inputStream 输入流
     * @return Workbook
     * @throws IOException IOException
     */
    public static Workbook getWorkbook(String fileName, InputStream inputStream) throws IOException {
        Workbook workbook;
        if (fileName.toLowerCase().endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (fileName.toLowerCase().endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new ExcelException("not support that file type except xlsx and xls");
        }
        return workbook;
    }
}
