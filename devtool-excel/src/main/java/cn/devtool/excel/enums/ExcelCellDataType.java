package cn.devtool.excel.enums;

import lombok.Getter;

/**
 * Excel单元格中值的类型
 *
 * @author chentiefeng
 * @created 2021/7/19 18:18
 */
@Getter
public enum ExcelCellDataType {
    /**
     * 默认类型，不做任何处理
     */
    DEFAULT("default", null),
    /**
     * excel中text类型
     */
    STRING("string", "TEXT"),
    /**
     * excel中数字类型
     */
    NUMBER("number", "#,##0.00"),
    /**
     * excel中日期格式
     */
    DATE("date", "yyyy-MM-dd"),
    /**
     * excel中时间格式
     */
    TIME("time", "HH:mm:ss"),
    /**
     * excel中时间格式
     */
    DATE_TIME("datetime", "yyyy-MM-dd HH:mm:ss"),
    /**
     * excel中布尔值
     */
    BOOLEAN("boolean", null);
    /**
     * 编码，用于前端传值确定类型
     */
    private String code;
    /**
     * 格式，主要用于日期
     */
    private String format;
    ExcelCellDataType(String code, String format) {
        this.code = code;
        this.format = format;
    }
}
