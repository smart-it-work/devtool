package cn.devtool.excel.model;

import cn.devtool.excel.enums.ExcelCellDataType;
import lombok.Data;
import org.apache.poi.ss.util.CellAddress;

/**
 * Excel的Column对象
 *
 * @author chentiefeng
 * @created  2021/4/21 16:48
 */
@Data
public class ExcelCell {
    /**
     * 行号，从1开始
     */
    private int rowNum;

    /**
     * 列号，从1开始
     */
    private int columnNum;
    /**
     * 值
     */
    private Object value;
    /**
     * Cell类型
     */
    private ExcelCellDataType dataType;
    /**
     * 宽度， 默认的宽度是2048
     */
    private int width;
    /**
     * address，如B2
     */
    private CellAddress address;
    /**
     * 公式
     */
    private String cellFormula;
    /**
     * 公司计算后的值格式
     */
    private ExcelCellDataType formulaResultDataType;
    /**
     * 单元格格式化后的值
     */
    private Object formatValue;
}
