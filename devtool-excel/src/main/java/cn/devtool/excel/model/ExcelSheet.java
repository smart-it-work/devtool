package cn.devtool.excel.model;

import lombok.Data;

import java.util.List;

/**
 * Excel的Sheet对象
 *
 * @author chentiefeng
 * @created  2021/4/21 16:45
 */
@Data
public class ExcelSheet {

    /**
     * sheet名称
     */
    private String sheetName;

    /**
     * Excel的行
     */
    private List<ExcelRow> rows;

    public ExcelSheet(String sheetName) {
        this.sheetName = sheetName;
    }
}
