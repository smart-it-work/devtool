package cn.devtool.excel.model;

import lombok.Data;

import java.util.List;

/**
 * Excel解析后的对象
 *
 * @author chentiefeng
 * @created  2021/4/21 16:44
 */
@Data
public class ExcelData {
    /**
     * 文件名
     */
    private String fileName;

    /**
     * sheets
     */
    private List<ExcelSheet> sheets;

    public ExcelData(String fileName) {
        this.fileName = fileName;
    }
}
