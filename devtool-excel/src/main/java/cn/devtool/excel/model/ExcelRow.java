package cn.devtool.excel.model;

import lombok.Data;

import java.util.List;

/**
 * Excel的Row对象
 *
 * @author chentiefeng
 * @created  2021/4/21 16:45
 */
@Data
public class ExcelRow {
    /**
     * 行号，从1开始
     */
    private int rowNum;
    /**
     * 高度, 默认的高度是285
     */
    private short height;
    /**
     * 列数据
     */
    private List<ExcelCell> cells;
}
