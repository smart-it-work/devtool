package cn.devtool.excel.model;

import lombok.Data;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * 合并单元格结果
 *
 * @author chentiefeng
 * @created  2021/4/22 10:51
 */
@Data
public class MergedRegionResult {
    private Boolean merged;
    private CellRangeAddress mergedRegion;

    public MergedRegionResult(Boolean merged){
        this.merged=merged;
    }
    public MergedRegionResult(Boolean merged, CellRangeAddress mergedRegion){
        this.merged=merged;
        this.mergedRegion=mergedRegion;
    }
}
