package cn.devtool.excel.exception;

/**
 * Excel 操作过程中的异常
 *
 * @author chentiefeng
 * @created 2021/4/21 17:21
 */
public class ExcelException extends RuntimeException {

    public ExcelException(String message) {
        super(message);
    }

    public ExcelException(String message, Throwable cause) {
        super(message, cause);
    }
}
