package cn.devtool.excel;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * CellStyle工具类
 *
 * @author chentiefeng
 * @created 2021/7/20 14:34
 */
public class CellStyleUtils {
    private CellStyleUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 创建CellStyle
     * @param wb Workbook
     * @return CellStyle
     */
    public static CellStyle createCellStyle(Workbook wb) {
        return wb.createCellStyle();
    }

    /**
     * 设置 水平排列
     * @param cellStyle  CellStyle
     * @param alignment HorizontalAlignment
     */
    public static void setAlignment(CellStyle cellStyle, HorizontalAlignment alignment) {
        cellStyle.setAlignment(alignment);
    }

    /**
     * 设置 垂直排列
     * @param cellStyle CellStyle
     * @param alignment VerticalAlignment
     */
    public static void setVerticalAlignment(CellStyle cellStyle, VerticalAlignment alignment) {
        cellStyle.setVerticalAlignment(alignment);
    }

    /**
     * 设置 背景色
     * @param cellStyle CellStyle
     * @param color 颜色
     */
    public static void setBackground(CellStyle cellStyle, IndexedColors color) {
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setFillForegroundColor(color.getIndex());
    }

    /**
     * 设置文本是否换行
     * @param cellStyle CellStyle
     * @param wrapTextFlag 文本换行标志
     */
    public static void setWrapText(CellStyle cellStyle, boolean wrapTextFlag) {
        cellStyle.setWrapText(wrapTextFlag);
    }

    /**
     * 设置右边框
     * @param cellStyle CellStyle
     * @param borderStyle BorderStyle
     * @param color 颜色
     */
    public static void setBorderRight(CellStyle cellStyle, BorderStyle borderStyle, IndexedColors color) {
        cellStyle.setBorderRight(borderStyle);
        cellStyle.setRightBorderColor(color.getIndex());
    }
    /**
     * 设置左边框
     * @param cellStyle CellStyle
     * @param borderStyle BorderStyle
     * @param color 颜色
     */
    public static void setBorderLeft(CellStyle cellStyle, BorderStyle borderStyle, IndexedColors color) {
        cellStyle.setBorderLeft(borderStyle);
        cellStyle.setLeftBorderColor(color.getIndex());
    }
    /**
     * 设置下边框
     * @param cellStyle CellStyle
     * @param borderStyle BorderStyle
     * @param color 颜色
     */
    public static void setBorderBottom(CellStyle cellStyle, BorderStyle borderStyle, IndexedColors color) {
        cellStyle.setBorderBottom(borderStyle);
        cellStyle.setBottomBorderColor(color.getIndex());
    }

    /**
     * 设置上边框
     * @param cellStyle CellStyle
     * @param borderStyle BorderStyle
     * @param color 颜色
     */
    public static void setBorderTop(CellStyle cellStyle, BorderStyle borderStyle, IndexedColors color) {
        cellStyle.setBorderTop(borderStyle);
        cellStyle.setTopBorderColor(color.getIndex());
    }

    /**
     * 设置行号
     * @param row 行
     * @param height 高度，新建文件的默认行高是285
     */
    public static void setRowHeight(Row row, short height) {
        row.setHeight(height);
    }

    /**
     * 设置列宽
     * @param cell 单元格
     * @param width 列宽
     */
    public static void setCellWidth(Cell cell, int width) {
        setCellWidth(cell.getSheet(), cell.getColumnIndex(), width);
    }

    /**
     * 设置列宽
     * @param sheet Sheet
     * @param columnIndex 列的索引
     * @param width 列宽，默认是2048
     */
    public static void setCellWidth(Sheet sheet, int columnIndex, int width) {
        sheet.setColumnWidth(columnIndex, width);
    }

    public static CellStyle titleStyle(Workbook wb) {
        CellStyle titleStyle = wb.createCellStyle();
        titleStyle.setAlignment(HorizontalAlignment.CENTER);
        titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        titleStyle.setFillPattern(FillPatternType.FINE_DOTS);
        titleStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        return titleStyle;
    }
}
