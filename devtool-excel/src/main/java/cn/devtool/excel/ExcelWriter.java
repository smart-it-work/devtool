package cn.devtool.excel;

import cn.devtool.excel.enums.ExcelCellDataType;
import cn.devtool.excel.model.ExcelCell;
import cn.devtool.excel.model.ExcelData;
import cn.devtool.excel.model.ExcelRow;
import cn.devtool.excel.model.ExcelSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

/**
 * Excel写入类
 *
 * @author chentiefeng
 * @created 2021/7/19 14:32
 */
public class ExcelWriter {
    private ExcelWriter() {
        throw new IllegalStateException("Utility class");
    }
    private static final String DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    private static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";
    /**
     * 写入Excel数据到 文件
     * @param file 文件
     * @param excelData Excel数据
     * @throws IOException IOException
     */
    public static void write(File file, ExcelData excelData) throws IOException {
        OutputStream outputStream = new FileOutputStream(file);
        write(outputStream, excelData);
    }
    /**
     * 写入Excel数据到 输出流
     * @param output 输出流
     * @param excelData excel数据
     * @throws IOException IOException
     */
    public static void write(OutputStream output, ExcelData excelData) throws IOException {
        try(SXSSFWorkbook workbook = new SXSSFWorkbook(1000)) {
            for (ExcelSheet sheet: excelData.getSheets()) {
                Sheet tmpSheet = workbook.createSheet(sheet.getSheetName());
                for (ExcelRow row: sheet.getRows()) {
                    Row tmpRow = tmpSheet.createRow(row.getRowNum()-1);
                    CellStyleUtils.setRowHeight(tmpRow, row.getHeight());
                    for (ExcelCell cell: row.getCells()) {
                        createCell(tmpRow, cell);
                    }
                }
            }
            workbook.write(output);
            output.flush();
        } finally {
            if (Objects.nonNull(output)) {
                output.close();
            }
        }
    }

    private static void createCell(Row tmpRow, ExcelCell cell) {
        Cell tmpCell = tmpRow.createCell(cell.getColumnNum()-1);
        if (cell.getWidth() > 0) {
            tmpCell.getSheet().setColumnWidth(tmpCell.getColumnIndex(), cell.getWidth());
        }
        if (Objects.nonNull(cell.getCellFormula())) {
            tmpCell.setCellFormula(cell.getCellFormula());
        } else {
            setCellValue(cell, tmpCell);
        }
        if (cell.getRowNum() == 1) {
            tmpCell.setCellStyle(CellStyleUtils.titleStyle(tmpCell.getSheet().getWorkbook()));
        }
    }

    /**
     * 设置Cell的值
     * @param cell ExcelCell
     * @param tmpCell Cell
     */
    private static void setCellValue(ExcelCell cell, Cell tmpCell) {
        String cellValue = getCellValue(cell);
        if (Objects.isNull(cellValue)) {
            return;
        }
        switch (cell.getDataType()){
            case DATE:
                if (cellValue.length() == DEFAULT_DATETIME_FORMAT.length()) {
                    cellValue = cellValue.substring(0, DEFAULT_DATE_FORMAT.length());
                }
                tmpCell.setCellValue(LocalDate.parse(cellValue, DateTimeFormatter.ofPattern(ExcelCellDataType.DATE.getFormat())));
                tmpCell.setCellStyle(getDataCellStyle(tmpCell.getSheet(), ExcelCellDataType.DATE.getFormat()));
                break;
            case TIME:
                if (cellValue.length() == DEFAULT_DATETIME_FORMAT.length()) {
                    cellValue = cellValue.substring(DEFAULT_DATE_FORMAT.length()+1);
                }
                tmpCell.setCellValue(cellValue);
                tmpCell.setCellStyle(getDataCellStyle(tmpCell.getSheet(), ExcelCellDataType.TIME.getFormat()));
                break;
            case DATE_TIME:
                tmpCell.setCellValue(LocalDateTime.parse(cellValue, DateTimeFormatter.ofPattern(ExcelCellDataType.DATE_TIME.getFormat())));
                tmpCell.setCellStyle(getDataCellStyle(tmpCell.getSheet(), ExcelCellDataType.DATE_TIME.getFormat()));
                break;
            case NUMBER:
                tmpCell.setCellValue(Double.parseDouble(cellValue));
                tmpCell.setCellStyle(getDataCellStyle(tmpCell.getSheet(), ExcelCellDataType.NUMBER.getFormat()));
                break;
            case BOOLEAN:
                tmpCell.setCellValue(Boolean.parseBoolean(cellValue));
                break;
            case STRING:
                tmpCell.setCellValue(cellValue);
                tmpCell.setCellStyle(getDataCellStyle(tmpCell.getSheet(), ExcelCellDataType.STRING.getFormat()));
                break;
            default:
                break;
        }
    }

    /**
     * 获取CellValue的值
     * @param cell ExcelCell
     * @return cell的字符串值
     */
    public static String getCellValue(ExcelCell cell) {
        String cellValue = null;
        if (Objects.nonNull(cell.getValue())) {
            if (cell.getValue() instanceof Double || cell.getValue() instanceof String || cell.getValue() instanceof Boolean) {
                cellValue = String.valueOf(cell.getValue());
            }
            if (cell.getValue() instanceof BigDecimal) {
                cellValue = ((BigDecimal)cell.getValue()).toPlainString();
            }
            if (cell.getValue() instanceof Date) {
                cellValue = new SimpleDateFormat(DEFAULT_DATETIME_FORMAT).format((Date)cell.getValue());
            }
            if (cell.getValue() instanceof LocalDateTime) {
                cellValue = ((LocalDateTime)cell.getValue()).format(DateTimeFormatter.ofPattern(DEFAULT_DATETIME_FORMAT));
            }
            if (cell.getValue() instanceof LocalDate) {
                cellValue = ((LocalDate)cell.getValue()).format(DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT));
            }
            if (cell.getValue() instanceof LocalTime) {
                cellValue = ((LocalTime)cell.getValue()).format(DateTimeFormatter.ofPattern(DEFAULT_TIME_FORMAT));
            }
        }
        return cellValue;
    }

    /**
     * 获取Cell格式
     * @param sheet Sheet
     * @param dataFormat 数据格式
     * @return CellStyle
     */
    private static CellStyle getDataCellStyle(Sheet sheet, String dataFormat) {
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        DataFormat format = sheet.getWorkbook().createDataFormat();
        cellStyle.setDataFormat(format.getFormat(dataFormat));
        return cellStyle;
    }
}
