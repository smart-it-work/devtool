package cn.devtool.excel;


import cn.devtool.excel.model.ExcelCell;
import cn.devtool.excel.model.ExcelRow;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Excel数据转换器
 *
 * @author chentiefeng
 * @created 2021/4/22 10:40
 */
public class ExcelDataConverter {
    private ExcelDataConverter(){}
    /**
     * 合并单元格的表头，表头之间的拼接字符串
     */
    public static final String HEAD_SPLIT_CHAR = "|+|";

    /**
     * 转换Excel行数据为Map集合数据
     * @param rowList 行数据
     * @param headLineNum 表头行的行号（从1开始），从改行往后的行，都是数据
     * @return map集合
     */
    public static List<Map<String, Object>> convert2Map(List<ExcelRow> rowList, int headLineNum) {
        Map<Integer, String> headTitleMap = new HashMap<>(10);
        List<ExcelRow> headRowList = rowList.stream().filter(item -> item.getRowNum() <= headLineNum).collect(Collectors.toList());
        headRowList.forEach(tmpRow ->
            tmpRow.getCells().forEach(tmpCell -> {
                String cellValue = tmpCell.getValue() == null ? "" : String.valueOf(tmpCell.getValue());
                String tmpHeadTitle = headTitleMap.get(tmpCell.getColumnNum());
                if (Objects.isNull(tmpHeadTitle) || tmpHeadTitle.trim().length() == 0) {
                    headTitleMap.put(tmpCell.getColumnNum(), cellValue);
                } else {
                    headTitleMap.put(tmpCell.getColumnNum(), tmpHeadTitle.concat(HEAD_SPLIT_CHAR).concat(cellValue));
                }
            })
        );

        rowList = rowList.stream().filter(item -> item.getRowNum() > headLineNum).collect(Collectors.toList());
        return rowList.stream().map(tmpRow -> {
            List<ExcelCell> tmpCells = tmpRow.getCells();
            Map<String, Object> tmpValueMap = new LinkedHashMap<>();
            tmpCells.forEach(tmpCell ->
                    tmpValueMap.put(headTitleMap.get(tmpCell.getColumnNum()), tmpCell.getValue())
            );
            return tmpValueMap;
        }).collect(Collectors.toList());
    }
}
