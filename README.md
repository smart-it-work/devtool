# devtool

## 📚介绍
开发工具：个人基于实际开发经验总结而成

## 🛠️包含组件
一个Java开发工具类，对日常开发使用的通用组件进行封装，提供以下组件：

| 模块                |     介绍                                                                          |
| -------------------|---------------------------------------------------------------------------------- |
| [devtool-core](https://gitee.com/chentiefeng-thinker/devtool/blob/develop/docs/devtool-core.md) |     通用工具类，提供多种Utils类                                              |
| [devtool-spring-starter](https://gitee.com/chentiefeng-thinker/devtool/blob/develop/docs/devtool-spring-starter.md) | 基于Spring的一些通用封装，如：Spring容器工具类 |
| [devtool-mybatis-starter](https://gitee.com/chentiefeng-thinker/devtool/blob/develop/docs/devtool-mybatis-starter.md) | 基于Mybatis的一些通用封装，如：记录执行的Sql |
| [devtool-worker](https://gitee.com/chentiefeng-thinker/devtool/blob/develop/docs/devtool-worker.md) | devtool对一些处理流程的封装，如：分组执行操作 |
| devtool-jpa-starter | 基于Jpa的一些通用封装，如：记录手动创建Sql，执行Sql |
| devtool-excel | 基于Excel操作的封装，如：excel解析，excel写入 |
| devtool-monitor | 基于监控的封装 |




## 📥使用说明

1. 添加依赖

   在项目的pom.xml的dependencies中加入以下内容:

   ``` xml
   		<dependency>
   			<groupId>cn.devtool</groupId>
   			<artifactId>模块</artifactId>
   			<version>版本</version>
   		</dependency>
   ```

2. 参考docs文件夹下，`模块名.md`文件进行使用

## 📦编译安装

1. 访问devtool的Gitee主页：https://gitee.com/chentiefeng-thinker/devtool 下载整个项目源码

2. 然后进入Hutool项目目录执行：

   ```sh
   ./devtool.sh install
   ```

3. 然后就可以使用Maven引入了。

## 🏗️添砖加瓦

### 🎋分支说明

devtool的源码分为4个分支，功能如下：

| 分支       | 作用                                                          |
|-----------|---------------------------------------------------------------|
| master | 主分支 |
| develop | 开发分支，默认为下个版本的SNAPSHOT版本，接受修改或pr                 |
| feature | feature分支会有一些新特性的添加 |
| release | release版本使用的分支，与中央库提交的jar一致，不接收任何pr或修改 |

### 🐞提供bug反馈或建议

提交问题反馈请说明正在使用的JDK版本呢、Hutool版本和相关依赖库版本。

- [Gitee issue](https://gitee.com/dromara/hutool/issues)
- [Github issue](https://github.com/dromara/hutool/issues)


### 🧬贡献代码的步骤

1. 在Gitee或者Github上fork项目到自己的repo
2. 把fork过去的项目也就是你的项目clone到你的本地
3. 修改代码（记得一定要修改develop分支）
4. commit后push到自己的库（develop分支）
5. 登录Gitee或Github在你首页可以看到一个 pull request 按钮，点击它，填写一些说明信息，然后提交即可。
6. 等待维护者合并

### 📐PR遵照的原则

devtool欢迎任何人为devtool添砖加瓦，贡献代码，不过维护者是一个强迫症患者，为了照顾病人，需要提交的pr（pull request）符合一些规范，规范如下：

1. 注释完备，尤其每个新增的方法应按照Java文档规范标明方法说明、参数说明、返回值说明等信息，必要时请添加单元测试，如果愿意，也可以加上你的大名。
2. Hutool的缩进按照Eclipse（~~不要跟我说IDEA多好用，维护者非常懒，学不会~~，IDEA真香，改了Eclipse快捷键后舒服多了）默认（tab）缩进，所以请遵守（不要和我争执空格与tab的问题，这是一个病人的习惯）。
3. 新加的方法不要使用第三方库的方法，Hutool遵循无依赖原则（除非在extra模块中加方法工具）。
4. 请pull request到`develop`分支。Hutool在5.x版本后使用了新的分支：`develop`是主分支，表示已经发布中央库的版本，这个分支不允许pr，也不允许修改。

-------------------------------------------------------------------------------



## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
